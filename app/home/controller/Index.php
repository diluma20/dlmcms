<?php declare(strict_types=1);

namespace app\home\controller;

use app\common\service\ArticleService;
use app\common\service\HouseService;
use app\common\service\CasesService;
use app\common\service\DesignerService;
use app\home\Diluma;
use app\common\service\TagService;
use think\facade\View;

class Index extends Diluma
{
    public function index()
    {
        $tagService = new TagService();
        $tagItems = $tagService->getTagList(1);
        $casesItems = (new CasesService())->getCasesBy(['is_rec'=>1], 8);
        $designerItems = (new DesignerService())->getDataList(9);
        $houseItems = (new HouseService())->getDataList(['is_rec'=>1], 12);
        $customerItems = (new ArticleService())->getArticleByCategory(23, ['is_rec'=>1]);
        $completedItems = (new ArticleService())->getArticleByCategory(24, ['is_rec'=>1]);
        $designerNum = (new DesignerService())->getDesignerNum();
        $caseNum = (new CasesService())->getCasesNum(8);
        $newsItems = (new ArticleService())->getArticleByCategory(1, ['is_rec'=>1], 5);
        $newsItems2 = (new ArticleService())->getArticleByCategory(2, ['is_rec'=>1], 4);
        View::assign('tag_list', $tagItems);
        View::assign('case_list', $casesItems);
        View::assign('designer_list', $designerItems);
        View::assign('house_list', $houseItems);
        View::assign('customer_list', $customerItems);
        View::assign('completed_list', $completedItems);
        View::assign('designer_num', $designerNum);
        View::assign('case_num', $caseNum);
        View::assign('news_list1', $newsItems);
        View::assign('news_list2', $newsItems2);
        return View::fetch();
    }

}
