<?php declare(strict_types=1);

namespace app\home\controller;

use app\common\model\Page;
use app\common\service\CasesService;
use app\common\service\DesignerService;
use app\common\service\CommonService;
use app\home\Diluma;
use app\Request;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\View;
use app\common\model\Nav;

class Designer extends Diluma
{
    /**
     * 首页
     * @return string
     */
    public function index(Request $request)
    {
        $service = new DesignerService();
        $page = $request->get('page', 1);
        $keywords = $request->get('keywords', '');
        $items = $service->getDataList(12, $keywords);
//        // header
          $this->getHeader(1, new Nav(), 3);
        View::assign('data', $items);
        $num = $service->getDesignerNum();
        View::assign('cur_location', (new CommonService())->getCurLocation(3));
        View::assign('designer_num', $num);
        return View::fetch();
    }

    /**
     * 详情页面
     * @param $id
     * @return string
     */
    public function detail($id)
    {
        $service = new DesignerService();
        $item = $service->getDetail($id);
        if (empty($item)) {
            return View::fetch('404.html');
        }
        // 上一篇
        $prevArticle = $service->getPrev($id);
        // 下一篇
        $nextArticle = $service->getNext($id);
        // header
        $this->getHeader(1, new \app\common\model\Designer(), 0, $id);
        // 设计师作品
        $designerCases = $service->getDesignerCases($id);
        $designerNum = (new DesignerService())->getDesignerNum();
        $caseNum = (new CasesService())->getCasesNum(8);
        View::assign('data', $item);
        View::assign('designer_cases', $designerCases);
        View::assign('cur_location', (new CommonService())->getCurLocation(3, $id));
        View::assign('prev', $prevArticle);
        View::assign('next', $nextArticle);
        View::assign('designer_num', $designerNum);
        View::assign('case_num', $caseNum);
        // 更新浏览量
        $service->views($id);
        return View::fetch();
    }

    /**
     * 服务流程
     * @return string
     */
    public function service(Request $request)
    {
        $id = 12;
        $model = new Page();
        $data = $model->findOrEmpty($id);
        // header
        $this->getHeader(1, new \app\common\model\Page(), $id);
        View::assign('cur_location', (new CommonService())->getCurLocation($id));
        View::assign('data', $data);
        return View::fetch();
    }
}