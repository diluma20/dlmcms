<?php declare(strict_types=1);

namespace app\home\controller;

use app\common\service\CommonService;
use app\home\Diluma;
use app\Request;
use think\facade\View;
use app\common\model\Page;

class Pages extends Diluma
{
    /**
     * 单页数据
     * @return string
     */
    public function index(Request $request)
    {
        $id = $request->param('id', 5, 'intval');
        $model = new Page();
        $data = $model->findOrEmpty($id);
        // header
        $this->getHeader(1, new \app\common\model\Page(), $id);
        View::assign('cur_location', (new CommonService())->getCurLocation($id));
        View::assign('data', $data);
        return View::fetch();
    }

}