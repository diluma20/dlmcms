<?php declare(strict_types=1);

namespace app\home\controller;

use app\common\service\ArticleService;
use app\common\service\CommonService;
use app\common\service\SiteService;
use app\home\Diluma;
use app\Request;
use think\facade\View;

class News extends Diluma
{
    /**
     * 新闻列表
     * @return string
     */
    public function index(Request $request)
    {
        $service = new ArticleService();
        $categoryId = $request->param('category_id', 228, 'intval');
        $keywords = $request->param('keywords', '');
        if (in_array($categoryId, [1,2])) {
            $items = $service->getArticleByCategory($categoryId, [], 12, $keywords);
        } else {
            $items = $service->getArticleByCategory([1,2], [], 12, $keywords);
        }
        // 最新
        $recNews = $service->getRecArticles(['status'=>1, 'is_rec'=>1], 5);
        // header
        $this->getHeader(1, new \app\common\model\Nav(), $categoryId);
        View::assign('cur_location', (new CommonService())->getCurLocation($categoryId));
        View::assign('category_id', $categoryId);
        View::assign('data', $items);
        View::assign('rec_news', $recNews);
        return View::fetch();
    }

    /**
     * 文章详情
     * @param $id
     * @return void
     */
    public function detail($categoryId = 0, $id, )
    {
        $service = new ArticleService();
        $item = $service->getArticleDetail($id);
        if (empty($item)) {
            return View::fetch('404.html');
        }
        // 上一篇
        $prevArticle = $service->getPrevArticle($id, $categoryId);
        // 下一篇
        $nextArticle = $service->getNextArticle($id, $categoryId);
        // 新闻推荐
        $recNews = $service->getRecArticles(['status'=>1, 'is_rec'=>1], 5);
        // header
        $this->getHeader(1, new \app\common\model\Article(), $item->category_id, $id);
        View::assign('cur_location', (new CommonService())->getCurLocation($item->category_id, $id));
        View::assign('data', $item);
        View::assign('rec_news', $recNews);
        View::assign('prev', $prevArticle);
        View::assign('next', $nextArticle);
        // 更新浏览量
        $service->views($id);
        return View::fetch();
    }

    /**
     * 知识库页面
     * @param $id
     * @return void
     */
    public function view($id, $categoryId = 0)
    {
        $service = new ArticleService();
        $item = $service->getArticleDetail($id);
        // 上一篇
        $prevArticle = $service->getPrevArticle($id, $categoryId);
        // 下一篇
        $nextArticle = $service->getNextArticle($id, $categoryId);
        // 新闻推荐
        $recArticles = $service->getRecArticles(['status'=>1, 'is_rec'=>1], 3);
        // 案例推荐
        $casesService = new CasesService();
        $casesItems = $casesService->getCasesList(0, true,6);
        // header
        $this->getHeader(1, new \app\common\model\Article(), $item->category_id, $id);
        View::assign('category', ['id'=>$item->category_id, 'name'=>$item->category->name]);
        View::assign('data', $item);
        View::assign('rec_articles', $recArticles);
        View::assign('rec_cases', $casesItems);
        View::assign('prev', $prevArticle);
        View::assign('next', $nextArticle);
        // 更新浏览量
        $service->views($id);
        return View::fetch();
    }



}