<?php declare(strict_types=1);

namespace app\home;

use app\BaseController;
use app\common\service\SiteService;
use think\facade\View;

class Diluma extends BaseController
{
    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        if (isMobile()) {
            return $this->redirectTo((string)url('/mobile/index/index'));
        }
        $this->getNav();
        $this->getWeblink(); // 友情链接
        $isHomePage = $this->isHomePage();
        View::assign('is_home_page', $isHomePage);
    }


}