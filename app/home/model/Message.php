<?php declare(strict_types=1);

namespace app\home\model;

use think\Model;

class Message extends Model
{
    /**
     * 开启自动完成
     * @var bool
     */
    protected $autoWriteTimestamp = true;

    /**
     * 设置自动完成字段
     * @var string
     */
    protected $createTime = 'create_time';

    // 关闭自动写入update_time字段
    protected $updateTime = false;
}