<?php declare(strict_types=1);

namespace app\job;

use think\queue\job;
use think\facade\Log;

class Msg
{
    public function fire(Job $job, $data)
    {
        $str = '<table style="border-collapse: collapse; width: 100%;">
                        <colgroup>
                          <col width="150">
                          <col width="150">
                          <col>
                        </colgroup>
                        <thead>
                          <tr>
                            <th>用户</th>
                            <th>联系方式</th>
                            <th>信息</th>
                          </tr> 
                        </thead>
                        <tbody>
                          <tr>
                            <td>'. $data['username'] .'</td>
                            <td>'. $data['contact'] .'</td>
                            <td>地区：'.$data['area'].'，面积：'.$data['area_num'].'，备注：'.$data['remarks'].'</td>
                          </tr>
                        </tbody>
                      </table>';
        $res = sendMail(config('app.receive_email'), '报价留言', $str);
        if ($res['code'] == 1) {
            Log::error("邮箱发送失败，error:" .$res['msg']);
        }
        // 任务处理失败，删除任务
        $job->delete();
    }
}