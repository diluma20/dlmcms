<?php
declare (strict_types = 1);

namespace app;

use app\common\service\SiteService;
use app\common\service\WeblinkService;
use app\common\service\CommonService;
use think\App;
use think\exception\HttpResponseException;
use think\exception\ValidateException;
use think\facade\View;
use think\Validate;

/**
 * 控制器基础类
 */
abstract class BaseController
{
    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    protected $middleware = [];

    /**
     * 构造方法
     * @access public
     * @param  App  $app  应用对象
     */
    public function __construct(App $app)
    {
        $this->app     = $app;
        $this->request = $this->app->request;
        // 控制器初始化
        $this->initialize();
    }

    // 初始化
    protected function initialize()
    {
        $this->getSiteConfig();
        View::assign("keywords", $this->request->param('keywords', ''));
        View::assign("controller", strtolower($this->request->controller()) ?? 'index');
        View::assign('quality_list', (new SiteService())->getChildNav(1));
        View::assign('about_list', (new SiteService())->getChildNav(271));
    }

    /**
     * 验证数据
     * @access protected
     * @param  array        $data     数据
     * @param  string|array $validate 验证器名或者验证规则数组
     * @param  array        $message  提示信息
     * @param  bool         $batch    是否批量验证
     * @return array|string|true
     * @throws ValidateException
     */
    protected function validate(array $data, string|array $validate, array $message = [], bool $batch = false)
    {
        if (is_array($validate)) {
            $v = new Validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                [$validate, $scene] = explode('.', $validate);
            }
            $class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
            $v     = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }

        $v->message($message);

        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }

        return $v->failException(true)->check($data);
    }

    /**
     * 获取导航菜单
     * @return void
     */
    public function getNav()
    {
        $siteService = new \app\common\service\SiteService();
        $navItems = $siteService->getNavList();
        $controller = strtolower($this->request->controller());
        foreach ($navItems as $item) {
            if (!empty($item['linkurl'])) {
                $linkurlArr = explode('/', $item['linkurl']);
                if ($controller == $linkurlArr[1]) {
                    $item['height'] = true;
                } else {
                    $item['height'] = false;
                }
                $item['linkurl'] = SITE_URL . DS . $item['linkurl'];
            }
        }
        View::assign("nav",  $navItems);
    }

    /**
     * 自定义重定向
     * @param $args
     */
    public function redirectTo(...$args)
    {
        throw new HttpResponseException(redirect(...$args));
    }

    /**
     * 获取网站配置信息
     * @return void
     */
    public function getSiteConfig()
    {
        $siteService = new SiteService();
        $siteConfig = $siteService->getSiteConfig();
        View::assign("siteConfig",  $siteConfig);
    }

    /**
     * 判断是否首页
     * @return void
     */
    public function isHomePage()
    {
        $homePage = [
            'module' => 'home',
            'controller' => 'index',
            'action' => 'index',
        ];
        $currPage = [
            'module' => strtolower(app('http')->getName()),
            'controller' => strtolower($this->request->controller()),
            'action' => $this->request->action(),
        ];
        if (implode('/', $homePage) === implode('/', $currPage)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * seo标题 seo关键字 seo描述
     * @return void
     */
    public function getHeader($siteId, $model, $navId=0, $id=0)
    {
        $common = new CommonService();
        $headerStr = $common->getHeader($siteId, $model, $navId, $id);
        View::assign('header', $headerStr);
    }

    /**
     * 友情链接
     * @return void
     */
    public function getWeblink()
    {
        $weblinkItems = (new WeblinkService())->getWeblink();

        View::assign('weblink_list', $weblinkItems);
    }

}
