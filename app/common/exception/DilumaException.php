<?php declare(strict_types=1);

namespace app\common\exception;

use think\Exception;

/**
 * 基础异常类
 */
class DilumaException extends Exception
{
    /**
     * @var array
     */
    public array $error = [
        'code' => 50003,
        'msg' => 'error',
        'data' => []
    ];

    /**
     * 构造函数
     * @param array $params 关联数组只应包含statusCode、msg和code，且不应该是空值
     */
    public function __construct(array $params = [])
    {
        parent::__construct();
        if (!is_array($params)) {
            return;
        }

        $this->error = array_merge($this->error, $params);
    }
}