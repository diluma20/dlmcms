<?php declare(strict_types=1);

namespace app\common\exception;

class ParamException extends DilumaException
{
    public array $error =[
        'code' => 50003,
        'msg' => 'error',
        'data' => []
    ];
}