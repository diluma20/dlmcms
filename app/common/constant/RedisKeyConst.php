<?php declare(strict_types=1);

namespace app\common\constant;

class RedisKeyConst
{

    /**
     * AccessToken
     * 类型: string
     */
    const CONFIG_APP_ACCESS_TOKEN = "config:app:access:token";
    /**
     * 认证会员年费
     * 类型: string
     */
    const CONFIG_APP_ANNUAL_FEE = "config:app:annual:fee";

    /**
     * 单页协议
     * 类型: string
     */
    const CONFIG_APP_PAGE_CONTENT = "config:app:page:%s";

    /**
     * 平台配置
     * 类型: string
     */
    const CONFIG_APP_PLATFORM_CONFIG = "config:app:platform:config:%s";

    /**
     * 获取用户信息
     * @param $userId
     * @return string
     */
    public static function getUserProfileKey($userId): string
    {
        return "user:{$userId}:profile";
    }

    /**
     * 获取用户扩展信息
     * @param $userId
     * @return string
     */
    public static function getUserExtendKey($userId): string
    {
        return "user:{$userId}:extend";
    }

    /**
     * 获取用户 openid
     * @param $userId
     * @return string
     */
    public static function getUserOpenIdKey($userId): string
    {
        return "user:{$userId}:openid";
    }
}