<?php declare(strict_types=1);

namespace app\common\traits;

use think\response\Json;

trait ReturnTrait
{
    /**
     * @describe 返回 layui 表格的数据格式
     * @param int $code
     * @param string $msg
     * @param array $data
     * @param int $count
     * @return Json
     */
    public static function ajaxResult(int $code = 0, string $msg = '', array $data = [], int $count=0): Json
    {
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'time' => time(),
            'data' => $data,
            'count'=> $count,
        ];
        return json($result);
    }

    /**
     * @describe 返回数据
     * @param int $code
     * @param string $msg
     * @param string $url
     * @param array $data
     * @return Json
     */
    public static function JsonReturn(int $code = 0, string $msg = "", string $url="", array $data = []): Json
    {
        $result = [
            'code' => $code,
            'msg' => $msg,
            "timestamp" => time(),
            'url' => $url,
            'data' => $data,
        ];
        return json($result);
    }
}