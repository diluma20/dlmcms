<?php declare(strict_types=1);

namespace app\common\model;

use think\Model;
use think\model\concern\SoftDelete;

class Cases extends Model
{
    use SoftDelete;
    protected $defaultSoftDelete = 0;
    protected string $deleteTime = "delete_time";
}