<?php declare(strict_types=1);

namespace app\common\model;

use app\common\helper\ConstBase;
use think\Model;

class Nav extends Model
{
    /**
     * 获取导航
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getNavList(){
        $items = self::order('sort asc')->select();
        return $this->_formatNav($items);
    }

    /**
     *  格式化
     * @param $nav
     * @param $id
     * @param $level
     * @return array
     */
    public function _formatNav($nav, $id = 0, $level = 0){
        static $navs = [];
        foreach ($nav as $item) {
            $item['nav_type_name'] = ConstBase::NAV_TYPE[$item['nav_type']];
            if ($item['parent_id'] == $id) {
                $item['level'] = $level+1;
                if($level == 0)
                {
                    $item['str'] = str_repeat('',$item['level']);
                }
                elseif($level == 2)
                {
                    $item['str'] = '&emsp;&emsp;&emsp;&emsp;'.'└ ';
                }
                elseif($level == 3)
                {
                    $item['str'] = '&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;'.'└ ';
                }
                else
                {
                    $item['str'] = '&emsp;&emsp;'.'└ ';
                }

                $navs[] = $item;
                $this->_formatNav($nav, $item['id'], $item['level']);
            }
        }
        return $navs;
    }

}