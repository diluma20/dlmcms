<?php declare(strict_types=1);

namespace app\common\service;

use app\common\model\Message;
use think\facade\Log;
class MsgService
{
    /**
     * 保存留言
     * @param $data
     * @return bool
     */
    public function updateOrInsert($data)
    {
        try {
            $model = new Message();
            $map = [
                'username' => $data['username'],
                'contact' => $data['contact'],
                'ip' => $data['ip']
            ];
            $item = $model->where($map)->find();
            if ($item) {
                $item->save($data);
            } else{
                $model->save($data);
            }

            return true;
        } catch (\Exception $e) {
            Log::error("app msg error:". $e->getMessage());
            return false;
        }
    }

    /**
     * 获取用户当天提交次数
     * @param $map
     * @return int
     */
    public function getMessageCount($map): int
    {
        $model = new Message();
        return $model->where($map)->whereDay('create_time')->count();
    }
}