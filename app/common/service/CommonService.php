<?php declare(strict_types=1);

namespace app\common\service;

use app\common\model\House;
use app\common\model\Nav;
use app\common\model\Site;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Request;

class CommonService
{
    /**
     * 栏目SEO头部调用
     * @param int $siteId
     * @param null $model
     * @param int $categoryId
     * @param int $id
     * @return string
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function getHeader($siteId = 1, $model = null, $categoryId = 0, $id = 0)
    {
        $headerStr = '';
        $siteModel = new Site();
        $item = $siteModel->field('site_flag,site_name,site_title,keywords,description,author')->where('id', $siteId)->find();
        $dlm_siteTitle    = $item->site_title ?? '';
        $dlm_generator    = $item->author ?? '';
        $dlm_author       = $item->author ?? '';
        $dlm_keywords     = $item->keywords ?? '';
        $dlm_description  = $item->description ?? '';

        // 定位到详情页
        if ($model && $categoryId && $id) {
            $row = $model->field('name,keywords,description')->where('id', $id)->find();
            if ($row) {
                $headerStr .= '<title>';
                if($row->name)
                    $headerStr .= $row->name .'-'. $dlm_siteTitle;
                else
                    $headerStr .= $dlm_siteTitle;
                $headerStr .= '</title>'.PHP_EOL;
                $headerStr .= '<meta name="author" content="'.$dlm_author.'" />'.PHP_EOL;
                $headerStr .= '<meta name="keywords" content="';
                if(!empty($row->keywords))
                    $headerStr .= $row->keywords;
                else
                    $headerStr .= $dlm_keywords;

                $headerStr .= '" />'.PHP_EOL;
                $headerStr .= '<meta name="description" content="';

                if(!empty($row->description))
                    $headerStr .= $row->description;
                else
                    $headerStr .= $dlm_description;

                $headerStr .= '" />'.PHP_EOL;
            }
        } else if ($model && $categoryId) {
            // 定位到栏目页
            $row = $model->field('name,keywords,description')->where('id', $categoryId)->find();
            if ($row) {
                $headerStr .= '<title>';
                if($row->name)
                    $headerStr .= $row->name.' - '.$dlm_siteTitle;
                else
                    $headerStr .= $dlm_siteTitle;
                $headerStr .= '</title>'.PHP_EOL;
                $headerStr .= '<meta name="author" content="'.$dlm_author.'" />'.PHP_EOL;
                $headerStr .= '<meta name="keywords" content="';
                if($row->keywords)
                    $headerStr .= $row->keywords;
                else
                    $headerStr .= $dlm_keywords;

                $headerStr .= '" />'.PHP_EOL;
                $headerStr .= '<meta name="description" content="';

                if($row->description)
                    $headerStr .= $row->description;
                else
                    $headerStr .= $dlm_description;

                $headerStr .= '" />'.PHP_EOL;
            }
        } else {
            $headerStr  .= '<title>'.$dlm_siteTitle.'</title>'.PHP_EOL;
            $headerStr .= '<meta name="author" content="'.$dlm_author.'" />'.PHP_EOL;
            $headerStr .= '<meta name="keywords" content="'.$dlm_keywords.'" />'.PHP_EOL;
            $headerStr .= '<meta name="description" content="'.$dlm_description.'" />'.PHP_EOL;
        }

        return $headerStr;
    }

    /**
     * 获取当前位置
     * @param   $cid     int     当前页面栏目id
     * @param   $id      int     当前页面文章id
     * @param   $sign    string  栏目之间分隔符
     * @return           string
     * @return void
     */
    public function getCurLocation($cid =  0, $id = 0, $sign='&nbsp;&gt;&nbsp;')
    {
        // 设置首页链接
        $pos_str = '<a href="'.SITE_URL.'">首页</a>';
        // 如果cid为空，获取串，否则视为首页
        if(!empty($cid))
        {
            // 获取当前栏目信息
            $siteService = new \app\common\service\SiteService();
            $data = $siteService->getNav($cid);
            if(empty($data)) {
                return $pos_str.$sign.'栏目不存在';
            } else {
                // 构成上级栏目字符
                if($data['parent_id'] != 0) {
                    $parentItem = $siteService->getNav($data['parent_id']);
                    if (!empty($parentItem['linkurl'])) {
                        $pos_str .= $sign.'<a href="'.$parentItem['linkurl'].'">'.$parentItem['name'].'</a>';
                    } else {
                        $pos_str .= $sign.'<a href="javascript:void(0)">'.$parentItem['name'].'</a>';
                    }

                    $pos_str .= $sign.$data['name'];
                } else {
                    return $pos_str.$sign.$data['name'];
                }
            }
        }

        return $pos_str;
    }

    /**
     * 获取当前位置
     * @param   $model   object   模型对象
     * @param   $cid     int     当前页面栏目id
     * @param   $id      int     当前页面文章id
     * @param   $sign    string  栏目之间分隔符
     * @return           string
     * @return void
     */
    public function getCurPos($model, $cid = 0, $id = 0, $sign='&nbsp;&gt;&nbsp;'): string
    {
        // 设置首页链接
        $pos_str = '<a href="'.SITE_URL.'">首页</a>';
        // 如果cid为空，获取串，否则视为首页
        $modelName = $model->getName();
        switch ($modelName){
            case "Article":
                $cateGory = "\\app\\common\\model\\ArticleCategory";
                $modelCateGory = new $cateGory;
                break;
            case "Cases":
                $cateGory = "\\app\\common\\model\\CasesCategory";
                $modelCateGory = new $cateGory;
                break;
            default:
                $modelCateGory = $model;
        }
        if($cid)
        {
            // 获取当前栏目信息
            $data = $modelCateGory->where('id', $cid)->field('id,name')->find();
            if(empty($data)) {
                $pos_str .= $sign.'栏目不存在';
            } else {
                if ($id) {
                    $pos_str .= $sign.'<a href="'.strtolower(url('index', ['category_id' => $cid])->build()).'">'.$data['name'].'</a>';
                } else {
                    $pos_str .= $sign.$data['name'];
                }

            }
        }
        if ($id) {
            $item = $model->where('id', $id)->find();
            if (empty($item)) {
                $pos_str .= $sign.'哎呀，它跑到火星上面去了';
            } else {
                $pos_str .= $sign.$item['name'];
            }
        }

        return $pos_str;
    }

    /**
     * 获取上一篇
     * @param $id
     * @return array
     */
    public function getPrev($model, $id): array
    {
        $item = $model->field('id,name')->where('id', '<', $id)->findOrEmpty()->toArray();
        if (!$item) {
            return [];
        }

        return $item;
    }

    /**
     * 获取下一篇
     * @param $id
     * @return array
     */
    public function getNext($model, $id): array
    {
        $item = $model->field('id,name')->where('id', '>', $id)->findOrEmpty()->toArray();
        if (!$item) {
            return [];
        }

        return $item;
    }

    /**
     * 更新浏览量
     * @param $id
     * @return void
     */
    public function views($model, $id)
    {
        return $model->where( 'id', $id)->setInc('views');
    }

    /**
     * 获取县区
     * @return void
     */
    public function getAreaList()
    {
        $items = (new House())->field('area')->group('area')->select();
        return $items;
    }
}