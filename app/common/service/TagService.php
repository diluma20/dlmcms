<?php declare(strict_types=1);

namespace app\common\service;

use app\common\model\Tag;
use think\Exception;
use think\facade\Log;

class TagService
{

    /**
     * 获取Tag标签
     * @param int $type 类型
     * @return void
     */
    public function getTagList(int $type = 0)
    {
        $model = new Tag();
        return $model
            ->field('id,name')
            ->where(function ($query) use($type){
                if($type){
                    $query->where('type', $type);
                }
                $query->where('status', 1);
            })
            ->order(['id'=>'asc'])
            ->select();
    }
    /**
     * 获取标签名称
     * @param $ids
     * @return void
     */
    public function getTagName($ids){
        if (empty($ids)) {
            return [];
        }
        $model = new Tag();
        return $model->whereIn('id', $ids)->column('name');
    }
}