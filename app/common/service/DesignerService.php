<?php declare(strict_types=1);

namespace app\common\service;
use app\common\model\Cases;
use app\common\model\Designer;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Paginator;

class DesignerService
{

    /**
     * 获取设计师
     * @param int $pageSize 条数
     * @return void
     */
    public function getDataList(int $pageSize = DEFAULT_PAGE_SIZE, ...$args)
    {
        $model = new Designer();
        $items = $model
            ->where(function ($query) use ($args) {
                if ($args) {
                    foreach ($args as $arg) {
                        if ($arg) {
                            $query->where('name', $arg);
                        }
                    }
                }
            })
            ->order(['id'=>'asc'])
            //->paginate(['list_rows'=>$pageSize, 'page'=>$page])
            ->paginate($pageSize)
            ->each(function ($item) {
                $item['case_num'] = $this->getCasesNum($item->id);
                return $item;
            });
        return $items;
    }

    /**
     * 获取设计师作品个数
     * @param int $designerId
     * @return int
     * @throws DbException
     */
    public function getCasesNum(int $designerId)
    {
        $model = new Cases();
        return $model->where( 'designer_id', $designerId)->count();
    }

    /**
     * 获取设计师信息
     * @param $id
     * @return void
     */
    public function getDesigner($id){
        if (!$id) {
            return [];
        }
        $model = new Designer();
        return $model->whereIn('id', $id)->find();
    }

    /**
     * 获取设计师作品
     * @param $designerId
     * @return void
     * @throws DbException
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     */
    public function getDesignerCases($designerId, $pageSize = 3){
        if (!$designerId) {
            return [];
        }
        $model = new Cases();
        $items = $model->where('designer_id', $designerId)->limit($pageSize)->select()
            ->each(function ($item) {
            $item['tag_name'] = (new TagService())->getTagName($item->tag_ids);
            return $item;
        });

        return $items;
    }

    /**
     * 获取设计师名称
     * @param $ids
     * @return void
     */
    public function getDesignerName($ids){
        if (empty($ids)) {
            return [];
        }
        $model = new Designer();
        return $model->whereIn('id', $ids)->column('name');
    }

    /**
     * 统计设计师个数
     * @return int
     * @throws \think\db\exception\DbException
     */
    public function getDesignerNum()
    {
        return (new Designer())->count();

    }

    /**
     * 详情页
     * @param $id
     * @return void
     */
    public function getDetail($id)
    {
        $model = new Designer();
        $item = $model->find($id);
        return $item;
    }

    /**
     * 获取某个字段得内容
     * @param array $map
     * @return void
     */
    public function getColumn(array $map, $column)
    {
        $model = new Designer();
        return $model->where($map)->value($column);
    }

    /**
     * 获取上一篇
     * @param $id
     * @return array
     */
    public function getPrev($id)
    {
        $model = new Designer();
        $item = $model->field('id,name')->where('id', '<', $id)->findOrEmpty()->toArray();
        if (!$item) {
            return [];
        }

        return $item;
    }

    /**
     * 获取下一篇
     * @param $id
     * @return array
     */
    public function getNext($id)
    {
        $model = new Cases();
        $item = $model->field('id,name')->where('id', '>', $id)->findOrEmpty()->toArray();
        if (!$item) {
            return [];
        }

        return $item;
    }

    /**
     * 更新浏览量
     * @param $id
     * @return array
     */
    public function views($id)
    {
        $model = new Designer();
        return $model->where( 'id', $id)->setInc('views');
    }
}