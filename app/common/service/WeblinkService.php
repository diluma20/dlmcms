<?php declare(strict_types=1);

namespace app\common\service;

use app\common\model\Weblink;
use think\Exception;
use think\facade\Log;

class WeblinkService
{

    /**
     * 获取标签名称
     * @param $ids
     * @return void
     */
    public function getWeblink(){
        $model = new Weblink();
        return $model->field('name,link_url')->select();
    }
}