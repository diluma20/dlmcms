<?php declare(strict_types=1);

namespace app\common\service;

use app\common\helper\ConstBase;
use app\common\model\CasesCategory;
use app\common\model\Cases;
use app\common\model\Tag;
use think\db\exception\DbException;
use think\facade\Log;
use think\Paginator;

class CasesService
{
    /**
     * 列表
     * @param int $categoryId
     * @param bool $isRec
     * @param int $pageSize
     * @param ... $args 可变参数
     * @return Paginator
     * @throws DbException
     */
    public function getCasesList(int $categoryId = 0, bool $isRec = false, int $pageSize = 9, $page = 1, ...$args)
    {
        $model = new Cases();
        return $model
            ->field(['id','name','category_id','thumb_url','designer_id','tag_ids','area','house','house_type'])
            ->where(['status'=>1])
            ->where(function ($query) use ($categoryId, $isRec, $args) {
                if($categoryId){
                    $query->where('category_id', $categoryId);
                }
                if($isRec){
                    $query->where('is_rec', 1);
                }
                // 可变参数
                if ($args) {
                    foreach ($args as $arg) {
                        if ($arg) {
                            $query->where('name|house|address', 'like', $arg. '%');
                        }
                    }
                }
            })
            ->order(['sort'=>'desc','id'=>'desc'])
            ->paginate(['list_rows'=>$pageSize, 'page'=>$page])
            ->each(function($item) {
                $item['designer'] = (new DesignerService())->getDesigner($item->designer_id);
                $item['tag_name'] = (new TagService())->getTagName($item->tag_ids);
                return $item;
            });
    }

    /**
     * 根据指定条件获取案例
     * @param array $map
     * @param int $pageSize
     * @return Paginator
     * @throws DbException
     */
    public function getCasesBy(array $map, int $pageSize = 8)
    {
        $model = new Cases();
        return $model
            ->field(['id','name','designer_id','category_id','thumb_url','designer_id','tag_ids','area','cost','house','house_type'])
            ->where(['status'=>1])
            ->where(function ($query) use ($map) {
                if($map){
                    foreach ($map as $key=>$value) {
                        $query->where($key, $value);
                    }
                }
            })
            ->order(['sort'=>'desc','id'=>'desc'])
            ->paginate($pageSize)
            ->each(function($item) {
                $item['designer'] = (new DesignerService())->getDesigner($item->designer_id);
                $item['tag_name'] = (new TagService())->getTagName($item->tag_ids);
                return $item;
            });
    }

    /**
     * 获取tag下的数据
     * @param int $type
     * @param int $pageSize
     * @return Paginator
     * @throws DbException
     */
    public function getCasesByTag(int $type, int $pageSize = 8)
    {
        $model = new Cases();
        $tagItems = (new TagService())->getTagList($type);
        $items = [];
        if (!empty($tagItems)) {
            $tagItems = $tagItems->each(function ($item) use ($model, $pageSize) {
                $item['case_list'] = $model
                    ->field(['id','name','category_id','thumb_url','designer_id','tag_ids','area','cost','house','house_type'])
                    ->where('find_in_set(:tag_ids, tag_ids)', ['tag_ids'=>$item->id])
                    ->where(['is_rec'=>1, 'status'=>1])
                    ->order(['sort'=>'desc','id'=>'desc'])
                    ->paginate($pageSize)
                    ->each(function($item) {
                        $item['designer_name'] = (new DesignerService())->getDesignerName($item->designer_id);
                        return $item;
                    })->toArray();
            });
            $items = $tagItems;
        }

        return $items;
    }

    /**
     * 获取定制案例
     * @return void
     */
    public function getDevList($pageSize = DEFAULT_PAGE_SIZE)
    {
        $model = new Cases();
        return $model
            ->field(['id','name','category_id','thumb_url','cover_url','tag_ids','online_date'])
            ->where('find_in_set(:dev_type, dev_type)', ['dev_type'=>1])
            ->where('status', 1)
            ->order(['sort'=>'desc','id'=>'desc'])
            ->paginate($pageSize)
            ->each( function($item){
                $tag = [];
                $ids = explode(',', $item->tag_ids);
                foreach ($ids as $key=>$id) {
                    $tag[$id] = (new TagService())->getTagName($id);
                }
                $item['tags'] = $tag;
                //$item['tag_names'] = (new TagService())->getTagName($item->tag_ids);
                return $item;
            });
    }

    /**
     * 详情
     * @param $id
     * @return void
     */
    public function getCasesDetail($id)
    {
        $model = new Cases();
        $item = $model->where('status', 1)->find($id);
        if ($item->tag_ids) {
            $item->tags = (new TagService())->getTagName($item->tag_ids);
        }
        if ($item->designer_id) {
            $item->designer = (new DesignerService())->getDesigner($item->designer_id);
        }
        if ($item->dev_type) {
            $item->dev = ConstBase::GetDevTypeName($item->dev_type);
        }
        return $item;
    }

    /**
     * 获取上一篇
     * @param $id
     * @param $categoryId
     * @return array
     */
    public function getPrevCases($id, $categoryId = 0)
    {
        $map[] = ['status', '=', 1];
        if ($categoryId) {
            $map[] = ['category_id', '=', $categoryId];
        }
        $model = new Cases();
        $item = $model->field('id,category_id,name')->where($map)->where('id', '<', $id)->findOrEmpty()->toArray();
        if (!$item) {
            return [];
        }

        return $item;
    }

    /**
     * 获取下一篇
     * @param $id
     * @param $categoryId
     * @return array
     */
    public function getNextCases($id, $categoryId = 0)
    {
        $map[] = ['status', '=', 1];
        if ($categoryId) {
            $map[] = ['category_id', '=', $categoryId];
        }
        $model = new Cases();
        $item = $model->field('id,category_id,name')->where($map)->where('id', '>', $id)->findOrEmpty()->toArray();
        if (!$item) {
            return [];
        }

        return $item;
    }

    /**
     * 更新浏览量
     * @param $id
     * @return array
     */
    public function views($id)
    {
        $model = new Cases();

        return $model->where( 'id', $id)->setInc('views');
    }

    /**
     * 获取个数
     * @param $categoryId
     * @return array
     */
    public function getCasesNum($categoryId = 0)
    {
        $model = new Cases();
        return $model->where( 'category_id', $categoryId)->count();
    }

    /**
     * 获取设计师作品个数
     * @param $id
     * @return array
     */
    public function getDesignerCasesNum($id)
    {
        $model = new Cases();
        return $model->where( 'designer_id', $id)->count();
    }

    /**
     * 分类列表
     * @return void
     */
    public function getCasesCategoryList()
    {
        $model = new CasesCategory();
        return $model
            ->field('id, name')
            ->order(['sort' => 'desc'])
            ->select();
    }

    /**
     * 获取分类
     * @param $categoryId
     * @return void
     */
    public function getCategoryBy($categoryId)
    {
        $model = new CasesCategory();
        return $model->where('id', $categoryId)->find();
    }

    /**
     * 获取分类名称
     * @param $categoryId
     * @return void
     */
    public function getCategoryName($categoryId)
    {
        $model = new CasesCategory();
        return $model->where('id', $categoryId)->value('name');
    }
}