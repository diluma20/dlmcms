<?php declare(strict_types=1);

namespace app\common\lib\Redis;

class RedisPackage
{
    private static $handler = null;
    private static $_instance = null;
    private static $options = [
        'host' => '127.0.0.1',
        'port' => 6379,
        'password' => '',
        'select' => 0,
        'timeout' => 0,
        'expire' => 0, // 全局缓存有效期（0为永久有效）
        'persistent' => 1,
        'prefix' => '',
    ];

    private function __construct($options = [])
    {
        if (!extension_loaded('redis')) {
            throw new \BadFunctionCallException('not support: redis');      //判断是否有扩展
        }
        if (!empty($options)) {
            self::$options = array_merge(self::$options, $options);
        }
        $func = self::$options['persistent'] ? 'pconnect' : 'connect';     //长链接
        self::$handler = new \Redis;
        self::$handler->$func(self::$options['host'], self::$options['port'], self::$options['timeout']);

        if ('' != self::$options['password']) {
            self::$handler->auth(self::$options['password']);
        }

        if (0 != self::$options['select']) {
            self::$handler->select(self::$options['select']);
        }
    }


    /**
     * @return \Redis\RedisPackage|null 对象
     * @param array $options
     * @return RedisPackage|null
     */
    public static function getInstance($options = [])
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self($options);
        }
        return self::$_instance;
    }

    /**
     * 禁止外部克隆
     */
    public function __clone()
    {
        trigger_error('Clone is not allow!',E_USER_ERROR);
    }

    /**
     * 判断 key 是否存在
     * @param string $key 键名
     * @return int|bool
     */

    public static function exists(string $key): int|bool
    {
        return self::$handler->exists($key);
    }

    /**
     * 写入缓存
     * @param string $key 键名
     * @param string $value 键值
     * @param int $exprie 过期时间 0:永不过期
     * @return bool
     */

    public static function set($key, $value, $exprie = 0)
    {
        if ($exprie == 0) {
            $set = self::$handler->set($key, $value);
        } else {
            $set = self::$handler->setex($key, $exprie, $value);
        }
        return $set;
    }

    /**
     * 读取缓存
     * @param string $key 键值
     * @return mixed
     */
    public static function get($key)
    {
        $fun = is_array($key) ? 'Mget' : 'get';
        return self::$handler->{$fun}($key);
    }
    /**
     * 删除缓存
     * @param string $key 键名
     * @return bool
     */

    public static function del($key)
    {

        return self::$handler->del($key);
    }
    /**
     * 获取值长度
     * @param string $key
     * @return int
     */
    public static function lLen($key)
    {
        return self::$handler->lLen($key);
        self::$handler->zRangeByScore();
    }

    /**
     * 将一个或多个值插入到列表头部
     * @param $key
     * @param $value
     * @return int
     */
    public static function LPush($key, $value)
    {
        return self::$handler->lPush($key, $value);
    }

    /**
     * 移出并获取列表的第一个元素
     * @param string $key
     * @return string
     */
    public static function lPop($key)
    {
        return self::$handler->lPop($key);
    }
    /**
     *向有序集合添加一个成员，或者更新已存在成员的分数
     * @param string $key
     * @param float $score
     * @param string $value
     * @return int
     */
    public static function zAdd($key, $score, $value){

        return self::$handler->zAdd( $key, $score, $value);
    }

    /**返回有序集合中指定分数区间的成员列表
     * @param string $key
     * @param float $start
     * @param float $end
     * @param array $options
     * @return array
     */
    public static function zRangeByScore($key, $start, $end, array $options= [] ){

        return  self::$handler->zRangeByScore($key, $start, $end, $options);

    }

    /**移除有序集中，指定分数（score）区间内的所有成员
     * @param string $key
     * @param float $start
     * @param float $end
     * @return int
     */
    public static function zRemRangeByScore($key, $start, $end ){

        return  self::$handler->zRemRangeByScore($key, $start, $end );
    }

    /**移除有序集中的一个成员
     * @param $key
     * @param $member1
     * @return int
     */
    public static function zRem($key, $member){

        return self::$handler->zRem($key, $member);
    }

    /*
     * 保存数组
     */
    public function setArr($key,$data){
        $this->set($key,json_encode($data));
    }

    /*
     * 获取数组
     */
    public function getArr($key){
        return json_decode($this->get($key));
    }

    /**获取key的剩余过期时间
     * @param $key
     * @return int
     */
    public static function ttl($key){
        return  self::$handler->ttl($key);
    }

    /**设置key的过期时间
     * @param $key
     * @param int $exprie
     * @return bool
     */
    public static function expire($key, int $exprie = 0){
        return self::$handler->expire($key, $exprie);
    }

    /**设置Hash以数组方式
     * @param $key
     * @param array $hashKeys
     * @return bool
     */
    public static function hMSet($key, array $hashKeys){
        return  self::$handler->hMSet($key, $hashKeys);
    }

    /**获取hash值，第二参数为或者的字段数组
     * @param $key
     * @param array $hashKeys
     * @return array
     */
    public static function hMGet($key, array $hashKeys){
        return  self::$handler->hMGet($key, $hashKeys);
    }

    /**获取Hash当前键的所有值
     * @param $key
     * @return array
     */
    public static function hGetAll($key){
        return  self::$handler->hGetAll($key);
    }

    /**设置Hash 单个设置
     * @param $key
     * @param $hashKey
     * @param $value
     * @return bool|int
     */
    public static function hSet($key, $hashKey, $value){
        return  self::$handler->hSet($key, $hashKey, $value);
    }

    /**
     * 获取hash值，单个获取
     * @param $key
     * @param $hashKey
     * @return string
     */
    public static function hGet($key, $hashKey){
        return  self::$handler->hGet($key, $hashKey);
    }

    /**
     * 设置Hash 字段自增
     * @param $key
     * @param $hashKey
     * @param $value
     * @return bool|int
     */
    public static function hIncrby($key, $hashKey, $value){
        return  self::$handler->hIncrby($key, $hashKey, $value);
    }
}