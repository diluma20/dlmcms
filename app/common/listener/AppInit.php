<?php declare(strict_types=1);

namespace app\common\listener;

class AppInit
{
    public function handle(){
        mb_internal_encoding('UTF-8'); // 设置 mbstring 字符编码
        $this->initSystemConst();
    }

    /**
     * 初始化系统常量
     * @author Diluma <1586498033@qq.com>
     * @date 20203/05/01
     */
    private function initSystemConst(){
        !defined('SITE_URL') && define('SITE_URL', env("dlm.site_url","https://www.848981.net")); //网址
        defined('VERSION') or define('VERSION', env("dlm.version","3.0")); //版本号
        defined('_NAME') or define('_NAME', env("dlm.name",'DLMCms-TP8')); //系统名称
        !defined('DS') && define('DS', DIRECTORY_SEPARATOR);
        !defined('DS_CONS') && define('DS_CONS', '\\');
        !defined('ADMIN_LOGIN_INFO') || define('ADMIN_LOGIN_INFO', env("dlm.admin_login_info")); // 管理员登录信息
        !defined('DEFAULT_PAGE_SIZE') && define('DEFAULT_PAGE_SIZE', 10);
    }
}