<?php declare(strict_types=1);

namespace app\common\helper;

class ConstBase
{
    // 导航类型
    const NAV_TYPE = [
        1 => '单页',
        2 => '文章',
        3 => '图片',
        4 => '商品',
        5 => '下载'
    ];

    // 文件类型
    const FILE_TYPE = [ 'exe', 'zip', 'rar', 'iso', 'gz', 'docx', 'xlsx', 'pdf' ];

    // 文件来源
    const FILE_SOURCE = [
        1 => '本站',
        2 => '外站',
    ];

    // 文章类型
    const ARTICLE_TYPE = [1=>'新闻', 2=>'知识', 3=>'技术类', 4=>'其他'];

    // 开发类型
    const DEV_TYPE = [1=>'定制', 2=>'二次开发', 3=>'迭代', 4=>'售后', 5=>'技术支持', 6=>'技术方案', 7=>'远程', 8=>'驻场'];

    /**
     * 获取分类名称
     * @param int|string $devTypes
     * @return void
     */
    public static function GetDevTypeName(int|string $devTypes)
    {
        if (is_int($devTypes)) {
            return self::DEV_TYPE[$devTypes];
        } else {
            $devTypesArr = explode(',', $devTypes);
            foreach ($devTypesArr as $key=>$index){
                $devTypesArr[$key] = self::DEV_TYPE[$index];
            }

            return $devTypesArr;
        }
    }
}