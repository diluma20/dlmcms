<?php declare(strict_types=1);

namespace app\mobile\controller;

use app\BaseController;
use app\common\service\CasesService;
use app\common\service\CommonService;
use app\Request;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\View;
use app\common\model\Nav;

class Cases extends BaseController
{
    /**
     * 首页
     * @return string
     */
    public function index(Request $request)
    {
        $service = new CasesService();
        $keywords = $request->param('keywords', '');
        $categoryId = $request->param('category_id', 8, 'intval');
        $page = $request->param('page', 1);
//        // 案例
        $items = $service->getCasesList($categoryId, false,12, $page, $keywords);
//        // header
          $this->getHeader(1, new Nav(), 2);
//        View::assign('category', ['id'=>$categoryId, 'name'=>$categoryName]);
        View::assign('cases_list', $items);
        $caseNum = (new CasesService())->getCasesNum(8);
        View::assign('cur_location', (new CommonService())->getCurLocation(2));
        View::assign('case_num', $caseNum);
        View::assign('keywords', $keywords);
        View::assign('category_name', $service->getCategoryName($categoryId));
        return View::fetch();
    }

    /**
     * 手机端异步请求
     * @return string
     */
    public function getDataList(Request $request)
    {
        $service = new CasesService();
        $keywords = $request->param('keywords', '');
        $categoryId = $request->param('category_id', 8, 'intval');
        $page = $request->param('page', 1);
//        // 案例
        $items = $service->getCasesList($categoryId, false,12, $page, $keywords);
        if (!empty($items)) {
            foreach ($items as &$cases) {
                $cases->linkurl = url('/mobile/cases/detail', ['category_id' => $cases->category_id, 'id' => $cases->id])->build();
            }
        }
        return json(['code'=>0, 'msg'=>'success', 'total_page'=>$items->lastPage(), 'data'=>$items->items()]);
    }

    /**
     * 详情
     * @param $id
     * @param int $categoryId
     * @return string
     * @throws DbException
     */
    public function detail($id, int $categoryId = 0)
    {
        $service = new CasesService();
        $item = $service->getCasesDetail($id);
        if (empty($item)) {
            return View::fetch('404.html');
        }
        if ($item->images_url) {
            $item->images_url = explode(';', rtrim($item->images_url, ';'));
        }
        // 上一篇
        $prevArticle = $service->getPrevCases($id, $categoryId);
        // 下一篇
        $nextArticle = $service->getNextCases($id, $categoryId);
        // 推荐案例
        $casesItems = $service->getCasesList(0, true,3);
        $this->getHeader(1, new \app\common\model\Cases(), $item->category_id, $id); // header
        View::assign('data', $item);
        View::assign('cur_location', (new CommonService())->getCurLocation(2, $id));
        View::assign('rec_cases', $casesItems);
        View::assign('prev', $prevArticle);
        View::assign('next', $nextArticle);
        View::assign('category_name', $service->getCategoryName($item->category_id));
        // 更新浏览量
        $service->views($id);
        return View::fetch();
    }

    /**
     * 猜你喜欢
     * @return string
     */
    public function like(Request $request)
    {
        $service = new CasesService();
        $items = $service->getCasesBy(['is_rec'=>1], 9);
        if (!empty($items)) {
            foreach ($items as &$cases) {
                $cases->linkurl = url('/home/cases/detail', ['category_id' => $cases->category_id, 'id' => $cases->id])->build();
            }
        }
        return json(['code'=>0, 'count'=>$items->total(), 'data'=>$items->items()]);
    }
}