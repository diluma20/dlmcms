<?php declare(strict_types=1);

namespace app\mobile\controller;

use app\BaseController;
use app\common\model\Page;
use app\common\service\CommonService;
use app\Request;
use think\facade\View;

class About extends BaseController
{
    /**
     * 关于我们
     * @return string
     */
    public function index(Request $request)
    {
        $id = $request->param('id', 3, 'intval');
        $model = new Page();
        $data = $model->findOrEmpty($id);
        // header
        $this->getHeader(1, new \app\common\model\Nav(), $id);
        View::assign('category_name', $data->name);
        View::assign('data', $data);
        return View::fetch();
    }

    /**
     * 企业文化
     * @return string
     */
    public function culture(Request $request)
    {

        return View::fetch();
    }

    /**
     * 发展历程
     * @return string
     */
    public function history(Request $request)
    {

        return View::fetch();
    }

    /**
     * 企业荣誉
     * @return string
     */
    public function honor(Request $request)
    {

        return View::fetch();
    }

}