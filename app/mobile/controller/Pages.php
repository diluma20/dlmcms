<?php declare(strict_types=1);

namespace app\mobile\controller;

use app\BaseController;
use app\common\service\CommonService;
use app\Request;
use think\facade\View;
use app\common\model\Page;

class Pages extends BaseController
{
    /**
     * 单页数据
     * @return string
     */
    public function index(Request $request)
    {
        $id = $request->param('id', 5, 'intval');
        $model = new Page();
        $data = $model->findOrEmpty($id);
        // header
        $this->getHeader(1, $model, $id);
        View::assign('cur_location', (new CommonService())->getCurLocation($id));
        View::assign('data', $data);
        View::assign('category_name', $model->where('id', $id)->value('name'));
        return View::fetch();
    }

}