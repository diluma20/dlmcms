<?php declare(strict_types=1);

namespace app\mobile\controller;

use app\BaseController;
use app\common\service\ArticleService;
use app\common\service\CasesService;
use app\common\service\DesignerService;
use app\common\service\TagService;
use think\facade\View;

class Index extends BaseController
{
    public function index()
    {
        $tagService = new TagService();
        $tagItems = $tagService->getTagList(1);
        $casesItems = (new CasesService())->getCasesBy(['is_rec'=>1], 8);
        $designerItems = (new DesignerService())->getDataList(9);
        $customerItems = (new ArticleService())->getArticleByCategory(23, ['is_rec'=>1]);
        $completedItems = (new ArticleService())->getArticleByCategory(24, ['is_rec'=>1]);
        $newsItems = (new ArticleService())->getArticleByCategory(2, ['is_rec'=>1], 5);
        View::assign('tag_list', $tagItems); // 风格标签
        View::assign('case_list', $casesItems); // 推荐案例
        View::assign('designer_list', $designerItems); // 设计师
        View::assign('customer_list', $customerItems); // 客户见证
        View::assign('completed_list', $completedItems); // 竣工
        View::assign('news_list', $newsItems);
        return View::fetch();
    }
}