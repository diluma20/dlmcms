<?php declare(strict_types=1);

namespace app\mobile\controller;

use app\BaseController;
use app\common\model\Page;
use app\common\service\ArticleService;
use app\common\service\CasesService;
use app\common\service\CommonService;
use app\common\service\DesignerService;
use app\Request;
use think\facade\View;
use app\common\model\Nav;

class Designer extends BaseController
{
    /**
     * 首页
     * @return string
     */
    public function index(Request $request)
    {
        $service = new DesignerService();
        $page = $request->get('page', 1);
        $keywords = $request->get('keywords', '');
        $items = $service->getDataList(12, $keywords);
//        // header
          $this->getHeader(1, new Nav(), 3);
        View::assign('data', $items);
        $num = $service->getDesignerNum();
        View::assign('designer_num', $num);
        View::assign('keywords', $keywords);
        View::assign('category_name', "设计团队");
        return View::fetch();
    }

    /**
     * 详情页面
     * @param $id
     * @return string
     */
    public function detail($id)
    {
        $service = new DesignerService();
        $item = $service->getDetail($id);
        if (empty($item)) {
            return View::fetch('404.html');
        }
        // 上一篇
        $prevArticle = $service->getPrev($id);
        // 下一篇
        $nextArticle = $service->getNext($id);
        $designerCases = $service->getDesignerCases($id, 12); // 设计师作品
        $designerNum = (new DesignerService())->getDesignerNum();
        $caseNum = (new CasesService())->getCasesNum(8);
        $designerCasesNum = (new CasesService())->getDesignerCasesNum($id);
        $this->getHeader(1, new Nav(), $item->category_id, $id);
        $customerItems = (new ArticleService())->getArticleByCategory(23, ['is_rec'=>1]);
        View::assign('customer_list', $customerItems); // 客户见证
        View::assign('data', $item);
        View::assign('designer_cases', $designerCases);
        View::assign('prev', $prevArticle);
        View::assign('next', $nextArticle);
        View::assign('designer_num', $designerNum);
        View::assign('case_num', $caseNum);
        View::assign('designer_cases_num', $designerCasesNum);
        View::assign('category_name', '设计团队');
        // 更新浏览量
        $service->views($id);
        return View::fetch();
    }

    /**
     * 异步请求获取数据
     * @return string
     */
    public function getDataList(Request $request)
    {
        $service = new DesignerService();
        $page = $request->get('page', 2);
        $items = $service->getDataList(12);
        if (!empty($items)) {
            foreach ($items as &$item) {
                $item->linkurl = url('/mobile/designer/detail', ['id' => $item->id])->build();
            }
        }
        return json(['code'=>0, 'msg'=>'success', 'total_page'=>$items->lastPage(), 'data'=>$items->items()]);
    }

    /**
     * 服务流程
     * @return string
     */
    public function service(Request $request)
    {
        $id = 12;
        $model = new Page();
        $data = $model->findOrEmpty($id);
        // header
        $this->getHeader(1, new \app\common\model\Page(), $id);
        View::assign('category_name', '服务流程');
        View::assign('data', $data);
        return View::fetch();
    }
}