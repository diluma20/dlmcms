<?php declare(strict_types=1);

namespace app\mobile\controller;

use app\BaseController;
use app\common\service\ArticleService;
use app\common\service\CommonService;
use app\Request;
use think\facade\View;
class News extends BaseController
{
    /**
     * 新闻列表
     * @return string
     */
    public function index(Request $request)
    {
        $service = new ArticleService();
        $categoryId = $request->param('category_id', 228, 'intval');
        $keywords = $request->param('keywords', '');
        if (in_array($categoryId, [1,2])) {
            $items = $service->getArticleByCategory($categoryId, [], 12, $keywords);
        } else {
            $items = $service->getArticleByCategory([1,2], [], 12, $keywords);
        }
        // 最新
        $recNews = $service->getRecArticles(['status'=>1, 'is_rec'=>1], 5);
        // header
        $this->getHeader(1, new \app\common\model\Nav(), $categoryId);
        View::assign('category_id', $categoryId);
        View::assign('data', $items);
        View::assign('category_name', $service->getCategoryName($categoryId));
        View::assign('rec_news', $recNews);
        return View::fetch();
    }

    /**
     * 文章详情
     * @param $id
     * @return void
     */
    public function detail($categoryId = 0, $id, )
    {
        $service = new ArticleService();
        $item = $service->getArticleDetail($id);
        if (empty($item)) {
            return View::fetch('404.html');
        }
        // 上一篇
        $prevArticle = $service->getPrevArticle($id, $categoryId);
        // 下一篇
        $nextArticle = $service->getNextArticle($id, $categoryId);
        // 新闻推荐
        $recNews = $service->getRecArticles(['status'=>1, 'is_rec'=>1], 5);
        // header
        $articleModel = new \app\common\model\Article();
        $this->getHeader(1, $articleModel, $item->category_id, $id);
        View::assign('cur_location', (new CommonService())->getCurPos($articleModel, $item->category_id, $id));
        View::assign('data', $item);
        View::assign('rec_news', $recNews);
        View::assign('category_name', $service->getCategoryName($item->category_id));
        View::assign('prev', $prevArticle);
        View::assign('next', $nextArticle);
        // 更新浏览量
        $service->views($id);
        return View::fetch();
    }

    /**
     * 异步请求获取数据
     * @return string
     */
    public function getDataList(Request $request)
    {
        $categoryId = $request->param('category_id');
        $service = new ArticleService();
        $items = $service->getArticleByCategory((int)$categoryId, [], 12);
        if (!empty($items)) {
            foreach ($items as &$item) {
                $item->linkurl = url('/mobile/news/detail', ['category_id' => $item->category_id, 'id' => $item->id])->build();
            }
        }
        return json(['code'=>0, 'msg'=>'success', 'total_page'=>$items->lastPage(), 'data'=>$items->items()]);
    }
}