<?php declare(strict_types=1);

namespace app\mobile\controller;

use app\BaseController;
use app\common\model\Nav;
use app\common\service\CasesService;
use app\common\service\CommonService;
use app\common\service\DesignerService;
use app\common\service\HouseService;
use app\Request;
use think\facade\View;
class House extends BaseController
{
    /**
     * 首页
     * @return string
     */
    public function index(Request $request)
    {
        $service = new HouseService();
        $page = $request->param('page', 1);
        $keywords = $request->param('keywords', '');
        $area = $request->param('area', '');
        $map = [];
        if ($area) {
            $map = ['area'=>$area];
        }
        $items = $service->getDataList($map,12, $page, $keywords);
        // header
        $this->getHeader(1, new Nav(), 220);
        View::assign('data', $items);
        View::assign('cur_location', (new CommonService())->getCurLocation(220));
        View::assign('cases_like', $this->guessLike());
        View::assign('category_name', '热装楼盘');
        View::assign('area', $area);
        View::assign('area_list', (new CommonService())->getAreaList());
        return View::fetch();
    }

    /**
     * 详情页面
     * @param $id
     * @return string
     */
    public function detail($id)
    {
        $service = new HouseService();
        $item = $service->getDetail($id);
        if (empty($item)) {
            return View::fetch('404.html');
        }
        $model = new \app\common\model\House();
        $commonService = new CommonService();
        // 上一篇
        $prevArticle = $commonService->getPrev($model, $id);
        // 下一篇
        $nextArticle = $commonService->getNext($model, $id);
        // header
        $this->getHeader(1, $model, 220, $id);
        View::assign('data', $item);
        //View::assign('designer_cases', $designerCases);
        View::assign('cur_location', (new CommonService())->getCurLocation(3, $id));
        View::assign('prev', $prevArticle);
        View::assign('next', $nextArticle);
        View::assign('category_name', '热装楼盘');

        // 获取相关案例
        $relevantCase = (new CasesService())->getCasesBy(['house'=>$item['name'], 'is_rec'=>1], 4);
        View::assign('relevant_case', $relevantCase);
        // 获取设计师
        $relevantDesigner = [];
        if ($relevantCase) {
            foreach ($relevantCase as $case) {
                $relevantDesigner[] = [
                    'designer_id' => $case['designer_id'],
                    'designer_name' => $case['designer']['name'],
                    'designer_cover' => $case['designer']['image_url'],
                    'designer_year' => $case['designer']['year'],
                    'designer_tag' => $case['designer']['tag'],
                    'designer_case_num' => (new CasesService())->getDesignerCasesNum($id)
                ];
            }
            $relevantDesigner = array_map('unserialize', array_unique(array_map('serialize', $relevantDesigner)));
        }
        View::assign('relevant_designer', $relevantDesigner);
        View::assign('cases_like', $this->guessLike());
        // 更新浏览量
        $commonService->views($model, $id);
        return View::fetch();
    }

    /**
     * 异步请求获取数据
     * @return string
     */
    public function getDataList(Request $request)
    {
        $area = $request->param('area', '');
        $map = [];
        if ($area) {
            $map = ['area'=>$area];
        }
        $service = new HouseService();
        $items = $service->getDataList($map,12);
        if (!empty($items)) {
            foreach ($items as &$item) {
                $item->linkurl = url('/mobile/house/detail', ['id' => $item->id])->build();
            }
        }
        return json(['code'=>0, 'msg'=>'success', 'total_page'=>$items->lastPage(), 'data'=>$items->items()]);
    }

    /**
     * 猜你喜欢
     * @return void
     * @throws \think\db\exception\DbException
     */
    public function guessLike()
    {
        return (new CasesService())->getCasesBy(['is_rec'=>1], 9);
    }
}