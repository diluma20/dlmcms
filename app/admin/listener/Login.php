<?php
declare (strict_types = 1);

namespace app\admin\listener;

use app\admin\model\Admin;

class Login
{
    /**
     * 事件监听处理
     *
     * @return mixed
     */
    public function handle($event)
    {
        $adminId = $event->getAdminId();
        $item = (new Admin())->getUser($adminId);
        $data = [
            'admin_user' => $item['username'].'/'.$item['nickname'],
            'route' => 'login',
            'method' => 'post',
            'request_data' => serialize(['behavior'=>'登录']),
            'ip' => get_client_ip()
        ];
        \think\facade\Db::name('admin_log')->save($data);
    }
}
