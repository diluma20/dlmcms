<?php declare(strict_types=1);

namespace app\admin\model;

use think\Model;
use think\model\concern\SoftDelete;

class Order extends Model
{
    use SoftDelete;
    protected $defaultSoftDelete = 0;
    protected string $deleteTime = "delete_time";

}