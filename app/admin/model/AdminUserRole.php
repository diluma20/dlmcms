<?php declare(strict_types=1);

namespace app\admin\model;

use think\model;

class AdminUserRole extends Model
{
    protected $table = 'dlm_admin_user';

    /**
     * 获取角色下的管理员
     * @param int $roleId
     * @return array
     * @author 的卢马
     * @date  2023/05/01
     */
    public function getRoleUser(int $roleId): array
    {
        $data = [];
        $items = self::where('role_id', $roleId)->select();
        if (!$items->isEmpty()) {
            foreach ($items as $item) {
                $data[] = [
                    'user_id' => $item->user_id,
                ];
            }
        }
        return $data;
    }

}