<?php declare(strict_types=1);

namespace app\admin\model;

use think\model;

class Menu extends Model
{
    /**
     * 系统菜单表
     * @var string
     */
    protected $table = 'dlm_admin_menu';

    /**
     * 菜单列表
     * @return void
     */
    public function getMenuList()
    {
        $list = self::order('sort asc')->select();
        return $this->_formatMenu($list);
    }

    /**
     * 格式化菜单
     * @param $menu
     * @param $id
     * @param $level
     * @return array
     */
    public function _formatMenu($menu, $id=0, $level=0){

        static $menus = [];
        foreach ($menu as $item) {
            if ($item['pid']==$id) {
                $item['level'] = $level+1;
                if($level == 0)
                {
                    $item['str'] = str_repeat('',$item['level']);
                }
                elseif($level == 2)
                {
                    $item['str'] = '&emsp;&emsp;&emsp;&emsp;'.'└ ';
                }
                elseif($level == 3)
                {
                    $item['str'] = '&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;'.'└ ';
                }
                else
                {
                    $item['str'] = '&emsp;&emsp;'.'└ ';
                }

                $menus[] = $item;
                $this->_formatMenu($menu, $item['id'], $item['level']);
            }
        }
        return $menus;
    }

    /**
     * 获取排序id
     * @return void
     */
    public function getSort()
    {
        return self::max('id');
    }
}