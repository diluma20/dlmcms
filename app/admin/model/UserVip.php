<?php declare(strict_types=1);

namespace app\admin\model;

use think\Model;

class UserVip extends Model
{
    /**
     * 获取会员状态
     * @param $userId
     * @return int|mixed
     */
    public function getUserVipExpire($userId): mixed
    {
        return $this->where('user_id', $userId)->value('expire_time') ? :0;
    }
}