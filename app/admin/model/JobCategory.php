<?php declare(strict_types=1);

namespace app\admin\model;

use think\Model;

class JobCategory extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s';
}