<?php declare(strict_types=1);

namespace app\admin\model;

use think\Model;

class UserIdcardAudit extends Model
{
    /**
     * 获取用户认证量
     * @param string $start
     * @param string $end
     * @return void
     */
    public function getUserAuthCount(string $start = '', string $end = ''){
        if (!empty($start) && !empty($end)) {
            $count = $this->where('create_time', '>=', strtotime($start))->where('create_time', '<=', strtotime($end. ' 23:59:59'))->distinct()->count('user_id') ? :0;
        } else {
            $count =  $this->distinct()->count() ? :0;
        }

        return $count;
    }
}