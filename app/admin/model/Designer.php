<?php declare(strict_types=1);

namespace app\admin\model;

use think\Model;
use think\model\concern\SoftDelete;

class Designer extends Model
{
    use SoftDelete;

    // 软删除字段的默认值
    protected $defaultSoftDelete = 0;
    // 软删除标记字段
    protected $deleteTime = 'delete_time';

    protected $dateFormat = 'Y-m-d H:i:s';
}