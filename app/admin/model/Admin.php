<?php declare(strict_types=1);

namespace app\admin\model;

use think\model;
use think\model\concern\SoftDelete;

class Admin extends Model
{
    // 管理员表
    protected $table = 'dlm_admin_user';

    use SoftDelete;
    // 软删除字段的默认值
    protected $defaultSoftDelete = 0;
    // 软删除标记字段
    protected $deleteTime = 'delete_time';

    protected $dateFormat = 'Y-m-d H:i:s';

    // 定义字段类型为 timestamp
    protected $type = [
        'login_time' => 'timestamp',
        'delete_time' => 'timestamp'
    ];

    /**
     * 获取管理员信息
     * @param int $id
     * @return array
     * @author 的卢马
     * @date  2023/05/01
     */
    public function getUser(int $id): array
    {
        $data = [];
        $user = self::where('status', 1)->find($id);
        if ($user) {
            $data = [
                'username' => $user->username,
                'nickname' => $user->nickname,
                'gender' => $user->gender,
                'avatar' => $user->avatar,
                'login_time' => $user->login_time,
            ];
        }
        return $data;
    }

}