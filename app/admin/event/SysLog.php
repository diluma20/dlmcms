<?php declare(strict_types=1);

namespace app\admin\event;

use app\admin\service\SystemLogService;

/**
 * 操作日志
 */
class SysLog
{
    protected array $except = [
        'insert', 'add', 'update', 'edit', 'delete', 'batchDelete', 'sort'
    ];

    public function handle(){
        if (request()->isAjax() || request()->isPost()) {
            $method = strtolower(request()->method());
            $action = strtolower(request()->action());
            if (in_array($action, $this->except)) {
                $url = substr(request()->url(), 0, 1000);
                $params = request()->param();
                if (isset($params['s'])) {
                    unset($params['s']);
                }
                if(!empty($params)){
                    //去除密码
                    foreach($params as $k=>$v){
                        if(stripos($k,"password")!==false){
                            unset($params[$k]);
                        }
                    }
                }
                $adminInfo = session('{ADMIN_LOGIN_INFO}');
                $data = [
                    'admin_user' => $adminInfo['username'].'/'.$adminInfo['nickname'],
                    'route' => $url,
                    'method' => $method,
                    'request_data' => serialize($params),
                    'ip' => get_client_ip()
                ];

                SystemLogService::instance()->save($data);
            }
        }
    }
}