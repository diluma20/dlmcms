<?php declare (strict_types = 1);

namespace app\admin\event;

/**
 * 记录管理员登录行为
 */
class Login
{
    /**
     * 记录管理员登录行为
     * @return mixed
     */
    public function getAdminId(): mixed
    {
        $item = session('{ADMIN_LOGIN_INFO}');
        return $item['admin_id'];
    }
}
