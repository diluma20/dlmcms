<?php declare(strict_types=1);

namespace app\admin;

use app\BaseController;
use app\admin\service\MenuService;
use think\facade\View;

class Diluma extends BaseController
{
    /**
     * 登录信息
     * @var array
     */
    protected $adminInfo;

    // 支付类型
    const PAY_TYPE = [
        '1' => '微信',
        '2' => '余额',
        '3' => '微信+余额'
    ];

    /**
     * 初始化
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        // 初始化配置
        $this->initConfig();
        // 初始化登录信息
        $this->initLogin();
    }

    /**
     * 初始化配置
     * @author 的卢马
     * @since: 2023/05/01
     */
    public function initConfig()
    {
        // 请求参数
        $param = $this->request->param();
        // 分页基础默认值
        defined('PAGE_SIZE') or define('PAGE_SIZE', isset($param['page_size']) ? $param['page_size'] : 10);
        defined('PAGE') or define('PAGE', isset($param['page']) ? $param['page'] : 1);
        defined('MAX_SIZE') or define('MAX_SIZE', 100000);
        // 站点名称
        View::assign("site_name", config('app.site_name'));
        View::assign("company_name", config('app.company_name'));
    }

    /**
     * 初始化登录信息
     * @author 的卢马
     * @since: 2024/2/25
     */
    public function initLogin()
    {
        // 登录用户信息
        $adminInfo = session('{ADMIN_LOGIN_INFO}');
        if (!empty($adminInfo)) {
            $this->adminInfo = $adminInfo;
            // 数据绑定
            View::assign("admin_id", $adminInfo['admin_id']);
            View::assign("admin_info", $this->adminInfo);

            // 菜单
            $menus = (new MenuService())->getMenuList();
            View::assign('menus',$menus);
            View::assign('account_record_type', config('app.account_type.field_value'));
            View::assign('pay_type', self::PAY_TYPE);
        } else {
            return redirect('/admin/login/login');
        }
    }
}