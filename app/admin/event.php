<?php
// 这是系统自动生成的event定义文件
return [
    'bind' => [
        'AdminLogin' => 'app\admin\event\Login', // 管理员登录日志
    ],

    'listen' => [
        'AppInit'  => [app\common\listener\AppInit::class], //初始化常量值
        'HttpRun'  => [],
        'HttpEnd'  => [app\admin\event\SysLog::class], //操作日志
        'LogLevel' => [],
        'LogWrite' => [],
        //注册监听类
        'AdminLogin' => ['app\admin\listener\Login'],
    ]
];
