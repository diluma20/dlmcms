<?php declare(strict_types=1);

namespace app\admin\service;

use app\admin\model\Download;
use app\admin\model\DownloadCategory;
use app\common\helper\ConstBase;
use think\facade\Log;

class DownloadService
{
    /**
     * 列表
     * @return void
     */
    public function getDownloadList($pageSize, $startDate, $endDate, $id, $keywords, $categoryId)
    {
        $model = new Download();
        return $model
            ->field(['id','name','category_id','thumb_url','is_rec','status','sort','download_url','download_num','original_name','views','file_type','file_size','create_time','tag_ids'])
            ->where(function ($query) use ($startDate, $endDate, $id, $keywords, $categoryId) {
                if($startDate){
                    $query->where('create_time', '>=', strtotime((string)$startDate));
                }
                if($endDate){
                    $query->where('create_time', '<=', strtotime($endDate. ' 23:59:59'));
                }
                if($id){
                    $query->where('id', $id);
                }
                if($keywords){
                    $query->where('name|keywords', 'like', $keywords .'%');
                }
                if($categoryId){
                    $query->where('category_id', $categoryId);
                }
            })
            ->order(['sort'=>'desc','id'=>'desc'])
            ->paginate($pageSize)
            ->each(function($item) {
                $item['category_name'] = $this->getCategoryName($item->category_id);
                return $item;
            });
    }

    /**
     * 更新数据
     * @param $data
     * @return void
     */
    public function updateOrInsert($data)
    {
        try {
            $model = new Download();
            if (isset($data['id'])) {
                $item = $model->find($data['id']);
                $item->save($data);
            } else {
                $model->save($data);
            }
            return json(['code'=>0, 'msg'=>'编辑成功']);
        } catch (\Exception $e) {
            Log::error("edit download error:". $e->getMessage());
            return json(['code'=>1, 'msg'=>'编辑失败，'. $e->getMessage()]);
        }
    }

    /**
     * 下载操作
     * @return void
     */
    public function action($id, $type){
        $model = new Download();
        try {
            $status = $type ? 1:0;
            $model->where('id', $id)->update(['status'=>$status, 'update_time'=>time()]);

            return json(['code'=>0, 'msg'=>'操作成功']);
        } catch (\Exception $e) {
            Log::error("action download {$id} error:". $e->getMessage());

            return json(['code'=>1, 'msg'=>'操作失败，'. $e->getMessage()]);
        }
    }

    /**
     * 删除记录
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        try {
            $ids = !is_array($id) ? [$id]:$id;
            $model = new Download();
            $model::destroy($id);

            return json(['code'=>0, 'msg'=>'删除成功']);
        } catch (\Exception $e) {
            Log::error("delete download " .json_encode($ids). " failed：".$e->getMessage());

            return json(['code'=>1, 'msg'=>'删除失败']);
        }
    }

    /**
     * 查询一条记录
     * @param $id
     * @return Article|array|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getOne($id)
    {
        $model = new Download();
        return $model->find($id);
    }

    /**
     * 分类列表
     * @return void
     */
    public function getDownloadCategoryList($pageSize = MAX_SIZE)
    {
        $model = new DownloadCategory();
        return $model->field('id,name,sort,status,update_time')->order(['sort' => 'desc', 'id' => 'desc'])->paginate($pageSize);
    }

    /**
     * 编辑分类
     * @param $data
     * @return \think\response\Json
     */
    public function categoryUpdateOrInsert($data)
    {
        try {
            $model = new DownloadCategory();
            if (isset($data['id'])) {
                $item = $model->find($data['id']);
                $item->save($data);
            } else {
                $model->save($data);
            }
            return json(['code'=>0, 'msg'=>'编辑成功']);
        } catch (\Exception $e) {
            Log::error("edit download category error:". $e->getMessage());
            return json(['code'=>1, 'msg'=>'编辑失败，'. $e->getMessage()]);
        }
    }

    /**
     * 删除分类
     * @param $id
     * @return void
     */
    public function categoryDelete($id)
    {
        $item = (new Download())->where(['status'=>1, 'category_id'=>$id, 'delete_time'=>0])->find();
        if($item){
            return json(['code'=>1, 'msg'=>'请先删除该分类下的记录']);
        }
        try {
            $ids = !is_array($id) ? [$id]:$id;
            $model = new DownloadCategory();
            $model::destroy($id);

            return json(['code'=>0, 'msg'=>'删除成功']);
        } catch (\Exception $e) {
            Log::error("delete download category " .json_encode($ids). " failed：".$e->getMessage());

            return json(['code'=>1, 'msg'=>'删除失败']);
        }
    }

    /**
     * 获取分类
     * @param $categoryId
     * @return void
     */
    public function getCategoryBy($categoryId)
    {
        $model = new DownloadCategory();
        return $model->where('id', $categoryId)->find();
    }

    /**
     * 获取分类名称
     * @param $categoryId
     * @return void
     */
    public function getCategoryName($categoryId)
    {
        $model = new DownloadCategory();
        return $model->where('id', $categoryId)->value('name');
    }
}