<?php declare(strict_types=1);

namespace app\admin\service;

use app\admin\model\AdminUserRole;
use app\admin\model\AdminRole;
use think\facade\Log;

class RoleService
{
    /**
     * 角色列表
     * @param $pageSize
     * @return void
     */
    public function getRoleList($pageSize)
    {
        $model = new AdminRole();
        return $model->where(['status'=>1])->order('id asc')->paginate($pageSize);
    }

    /**
     * 编辑角色
     * @param $data
     * @return void
     */
    public function edit($data)
    {
        $model = new AdminRole();
        try {
            $item = $model->find($data['id']);
            if (!$item) {
                return json(['code'=>1, 'msg'=>'角色不存在']);
            }
            $item->save($data);
            return json(['code'=>0, 'msg'=>'编辑成功']);
        } catch (\Exception $e) {
            $error = $e->getMessage();
            Log::error("edit role error:". $error);
            return json(['code'=>1, 'msg'=>$error]);
        }
    }

    /**
     * 添加角色
     * @param $data
     * @return void
     */
    public function insert($data)
    {
        $model = new AdminRole();
        try {
            $item = $model->where('name', $data['name'])->find();
            if ($item) {
                return json(['code'=>1, 'msg'=>'角色已存在，请勿重复添加']);
            }
            $model->save([
                'name'  =>  $data['name'],
                'desc' =>  $data['desc'] ?? '',
                'permission_ids' => $data['permission_ids']
            ]);
            return json(['code'=>0, 'msg'=>'添加成功']);
        } catch (\Exception $e) {
            $error = $e->getMessage();
            Log::error("insert role error:". $error);
            return json(['code'=>1, 'msg'=>$error]);
        }
    }

    /**
     * @param $id
     * @return AdminRole
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getOneBy($id): AdminRole
    {
        $model = new AdminRole();
        return $model->find($id);
    }

    /**
     * 删除角色
     * @param int $id
     * @return void
     */
    public function delete(int $id)
    {
        // 判断该角色下是否有管理员
        $userIds = (new AdminUserRole())->getRoleUser($id);
        if (!empty($userIds)) {
            return json(['code'=>1, 'msg'=>'该角色下还有管理员，请先删除所有管理员']);
        }
        $model = new AdminRole();
        try {
            $model::destroy($id);
            return json(['code'=>0, 'msg'=>'删除成功']);
        } catch (\Exception $e) {
            $error = $e->getMessage();
            Log::error("delete role error:". $e->getMessage());
            return json(['code'=>1, 'msg'=>"删除失败：". $error]);
        }
    }

    /**
     * 获取角色权限
     * @param $roleId
     * @return void
     */
    public function getUserPermissionIds($roleId){
        if (!$roleId) {
            return;
        }
        $model = new AdminRole();
        return $model->where(['id'=>$roleId, 'status'=>1])->value('permission_ids');
    }

}