<?php declare(strict_types=1);

namespace app\admin\service;

use app\admin\event\Login;
use app\admin\model\Nav;
use think\Exception;
use think\facade\Log;

class NavService
{
    /**
     * 列表
     * @return array
     */
    public function getNavList()
    {
        $model = new Nav();
        return $model->getNavList();
    }

    /**
     * 更新数据
     * @param $data
     * @return void
     */
    public function updateOrInsert($data)
    {
        if (empty($data)) {
            return false;
        }
        try {
            $model = new Nav();
            if (isset($data['id'])) {
                $item = $model->find($data['id']);
                $item->save($data);
            } else {
                $model->save($data);
            }
            return true;
        } catch (\Exception $e) {
            Log::error("exit nav failed:" .$e->getMessage());
            return false;
        }
    }

    /**
     * 更新排序
     * @param $data
     * @return void
     */
    public function sort($data)
    {
        try {
            $model = new Nav();
            if (isset($data['id'])) {
                $item = $model->find($data['id']);
                $item->save($data);
            } else {
                $model->save($data);
            }
            return json(['code'=>0, 'msg'=>'排序成功']);
        } catch (\Exception $e) {
            Log::error("sort nav failed:" .$e->getMessage());
            return json(['code'=>1, 'msg'=>'排序失败，' .$e->getMessage()]);
        }
    }

    /**
     * 删除记录
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        try {
            $model = new Nav();
            $model->where('id', $id)->delete();
            return json(['code'=>0, 'msg'=>'删除成功']);
        } catch (\Exception $e) {
            Log::error("delete nav failed:" .$e->getMessage());
            return json(['code'=>1, 'msg'=>'删除失败，' .$e->getMessage()]);
        }
    }

    /**
     * 查询一条记录
     * @param $id
     * @return void
     */
    public function getOne($id)
    {
        $model = new Nav();
        return $model->find($id);
    }

    /**
     * 查询记录
     * @param $name
     * @return void
     */
    public function getOneBy($name)
    {
        $model = new Nav();
        return $model->where('name', $name)->find();
    }

    /**
     * 获取排序id
     * @return void
     */
    public function getSort()
    {
        $model = new Nav();
        return $model->max('id') ? $model->max('id') + 1 : 1;
    }

    /**
     * 获取标签名称
     * @param $ids
     * @return void
     */
    public function getNavName($ids){
        $name = '';
        $model = new Nav();
        if (is_array($ids)) {
            $idsArr = $model->whereIn('id', $ids)->column('name');
            if (!empty($idsArr)) {
                $name = implode(',', $idsArr);
            }
        } else {
            $ids = explode(',', $ids);
            $idsArr = $model->whereIn('id', $ids)->column('name');
            if (!empty($idsArr)) {
                $name = implode(',', $idsArr);
            }
        }
        return $name;
    }
}