<?php declare(strict_types=1);

namespace app\admin\service;

use app\admin\model\Designer;
use think\facade\Log;

class DesignerService
{
    /**
     * 列表
     * @return void
     */
    public function getDataList($pageSize)
    {
        $model = new Designer();
        return $model->where(['delete_time'=>0])
            ->order('id desc')
            ->paginate($pageSize);
    }

    /**
     * 更新数据
     * @param $data
     * @return void
     */
    public function updateOrInsert($data)
    {
        if (empty($data)) {
            return false;
        }
        $model = new Designer();
        try {
            if (isset($data['id'])) {
                $item = $model->find($data['id']);
                $item->save($data);
            } else {
                $model->save($data);
            }
            return json(['code'=>0, 'msg'=>'编辑成功']);
        } catch (\Exception $e) {
            Log::error("edit designer error:". $e->getMessage());
            return json(['code'=>1, 'msg'=>'编辑失败，'. $e->getMessage()]);
        }
    }

    /**
     * 删除记录
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        $model = new Designer();
        return $model::destroy($id);
    }

    /**
     * 查询一条记录
     * @param $id
     * @return void
     */
    public function getOne($id)
    {
        $model = new Designer();
        return $model->find($id);
    }
}