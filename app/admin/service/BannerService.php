<?php declare(strict_types=1);

namespace app\admin\service;

use app\admin\model\Banner;
use think\facade\Log;

class BannerService
{
    /**
     * 列表
     * @return void
     */
    public function getBannerList($pageSize)
    {
        $model = new Banner();
        $items = $model->where(['delete_time'=>0])
            ->order('id desc')
            ->paginate($pageSize)
            ->each(function($item) {
                $item['category_name'] = (new BannerCategoryService())->getName($item->category_id);
                return $item;
            });

        return $items;
    }

    /**
     * 更新数据
     * @param $data
     * @return void
     */
    public function updateOrInsert($data)
    {
        if (empty($data)) {
            return false;
        }
        $model = new Banner();
        try {
            if (isset($data['id'])) {
                $item = $model->find($data['id']);
                $item->save($data);
            } else {
                $model->save($data);
            }
            return json(['code'=>0, 'msg'=>'编辑成功']);
        } catch (\Exception $e) {
            Log::error("edit banner error:". $e->getMessage());
            return json(['code'=>1, 'msg'=>'编辑失败，'. $e->getMessage()]);
        }
    }

    /**
     * 删除记录
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        $model = new Banner();
        return $model::destroy($id);
    }

    /**
     * 查询一条记录
     * @param $id
     * @return void
     */
    public function getOne($id)
    {
        $model = new Banner();
        return $model->find($id);
    }
}