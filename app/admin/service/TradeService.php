<?php declare(strict_types=1);

namespace app\admin\service;

use app\admin\model\Trade;
use think\facade\Log;

class TradeService
{
    /**
     * 列表
     * @return void
     */
    public function getTradeList()
    {
        $model = new Trade();
        return $model->where('is_delete', 0)->order(['sort','id'=>'desc'])->paginate(MAX_SIZE);
    }

    /**
     * 更新数据
     * @param $data
     * @return void
     */
    public function updateOrInsert($data)
    {
        if (empty($data)) {
            return false;
        }
        try {
            $model = new Trade();
            if (isset($data['id'])) {
                $item = $model->find($data['id']);
                $item->save($data);
            } else {
                $model->save($data);
            }
            return json(['code'=>0, 'msg'=>'编辑成功']);
        } catch (\Exception $e) {
            Log::error("edit trade error:". $e->getMessage());
            return json(['code'=>1, 'msg'=>'( ⊙ o ⊙ )！服务器反应不过来了，请稍后再试，'. $e->getMessage()]);
        }
    }

    /**
     * 删除记录
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        try {
            $model = new Trade();
            $tradeItems = $model->where(['parent_id'=>$id, 'status'=>0, 'is_delete'=>0])->find();
            if($tradeItems){
                return json(['code'=>1, 'msg'=>'该行业分类下含有子分类，禁止删除']);
            }
            $model->where('id', $id)->update([ 'status'=> 0, 'is_delete'=> 1, 'update_time'=>time()]);
        } catch (\Exception $e) {
            Log::error("delete trade failed:" .$e->getMessage());
            return json(['code'=>1, 'msg'=>$e->getMessage()]);
        }

        return json(['code'=>0, 'msg'=>'删除成功']);
    }

    /**
     * 查询一条记录
     * @param int $id
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getOne(int $id)
    {
        $model = new Trade();
        return $model->find($id);
    }

    /**
     * 获取行业列表
     * @param int $tradeId
     * @return mixed
     */
    public function getTrades(int $tradeId = 0)
    {
        $model = new Trade();
        if($tradeId){
            return  $model->field('id,name')->where('parent_id', $tradeId)->order(['id'=>'asc'])->select();
        } else{
            return  $model->field('id,name')->where('parent_id', 0)->order(['id'=>'asc'])->select();
        }
    }

    /**
     * 获取行业id
     * @param int $tradeId
     * @return mixed
     */
    public function getTradeParentId(int $tradeId = 0): mixed
    {
        $model = new Trade();
        return $model->where('id', $tradeId)->value('parent_id');
    }
}