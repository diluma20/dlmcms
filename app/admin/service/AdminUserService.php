<?php declare(strict_types=1);

namespace app\admin\service;

use app\admin\model\Admin as AdminModel;

class AdminUserService
{
    /**
     * 获取管理员状态
     * @return mixed
     */
    public function getAdminUserStatus(): mixed
    {
        $adminInfo = session('{ADMIN_LOGIN_INFO}');
        $model = new AdminModel();
        $item = $model->find($adminInfo['admin_id']);
        return $item->status;
    }
}