<?php declare(strict_types=1);

namespace app\admin\service;

use app\admin\model\Job;

class JobService
{
    /**
     * @return mixed
     */
    public function getJobList(){
        $model = new Job();
        return $model->where(['status'=>1, 'is_delete'=>0])->order(['sort','id'=>'desc'])->paginate();
    }
}