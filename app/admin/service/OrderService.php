<?php declare(strict_types=1);

namespace app\admin\service;

use app\admin\model\Order;
use think\facade\Log;
use think\facade\Db;

class OrderService
{
    /**
     * 列表
     * @return void
     */
    public function getOrderList($startDate, $endDate, $pageSize, $orderSn, $userId)
    {
        $userService = new UserService();
        $model = new Order();
        $items = $model
            ->where(function ($query) use ($startDate, $endDate, $orderSn, $userId){
                if($startDate){
                    $query->where('create_time', '>=', strtotime($startDate. ' 00:00:00'));
                }
                if($endDate){
                    $query->where('create_time', '<=', strtotime($endDate.' 23:59:59'));
                }
                if($orderSn){
                    $query->where('order_sn', $orderSn);
                }
                if($userId){
                    $query->where('user_id', $userId);
                }
            })
            ->order('id desc')
            ->paginate((int)$pageSize)->each(function($item) use ($userService){
                $userItem = $userService->getUserProfile($item['user_id']);
                $item['avatar'] = $userItem['avatar'];
                $item['nickname'] = $userItem['nickname'];
                return $item;
            });
        return $items;
    }

}