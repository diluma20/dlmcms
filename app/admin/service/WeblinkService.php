<?php declare(strict_types=1);

namespace app\admin\service;

use app\common\model\Weblink;
use think\facade\Log;

class WeblinkService
{
    /**
     * 列表
     * @return void
     */
    public function getWeblinkList($pageSize)
    {
        $model = new Weblink();
        return $model->order('id desc')->paginate($pageSize);
    }

    /**
     * 更新数据
     * @param $data
     * @return void
     */
    public function updateOrInsert($data)
    {
        try {
            $model = new Weblink();
            if (isset($data['id'])) {
                $item = $model->find($data['id']);
                $item->save($data);
            } else {
                $model->save($data);
            }
            return json(['code'=>0, 'msg'=>'编辑成功']);
        } catch (\Exception $e) {
            Log::error("edit weblink error:". $e->getMessage());
            return json(['code'=>1, 'msg'=>'编辑失败，'. $e->getMessage()]);
        }
    }

    /**
     * 删除记录
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        $model = new Weblink();
        return $model::destroy($id);
    }
}