<?php declare(strict_types=1);

namespace app\admin\service;

use app\common\model\Site;
use think\facade\Log;

class SiteService
{
    /**
     * 网站配置
     * @return mixed
     */
    public function getSiteConfig()
    {
        $model = new Site();
        return $model->find(1);
    }

    /**
     * 更新数据
     * @param $data
     * @return void
     */
    public function updateOrInsert($data)
    {
        try {
            $model = new Site();
            if (isset($data['id'])) {
                $item = $model->find($data['id']);
                $item->save($data);
            } else {
                $model->save($data);
            }
            return json(['code'=>0, 'msg'=>'编辑成功']);
        } catch (\Exception $e) {
            Log::error("edit site error:". $e->getMessage());
            return json(['code'=>1, 'msg'=>'编辑失败，'. $e->getMessage()]);
        }
    }
}