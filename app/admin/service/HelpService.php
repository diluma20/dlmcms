<?php declare(strict_types=1);

namespace app\admin\service;

use app\admin\model\Help;
use think\facade\Log;

class HelpService
{
    /**
     * 列表
     * @return void
     */
    public function getHelpList($pageSize)
    {
        $model = new Help();
        return $model->order('id desc')->paginate($pageSize);
    }

    /**
     * 更新数据
     * @param $data
     * @return void
     */
    public function updateOrInsert($data)
    {
        if (empty($data)) {
            return false;
        }
        $model = new Help();
        try {
            if (isset($data['id'])) {
                $item = $model->find($data['id']);
                $item->save($data);
            } else {
                $model->save($data);
            }
            return true;
        } catch (\Exception $e) {
            Log::error("edit error:". $e->getMessage());
            return false;
        }
    }

    /**
     * 删除记录
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        $model = new Help();
        return $model->where('id', $id)->delete();
    }

    /**
     * 查询一条记录
     * @param $id
     * @return void
     */
    public function getOne($id)
    {
        $model = new Help();
        return $model->find($id);
    }
}