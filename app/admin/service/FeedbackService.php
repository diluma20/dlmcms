<?php declare(strict_types=1);

namespace app\admin\service;

use app\admin\model\Feedback;
use think\facade\Log;

class FeedbackService
{
    /**
     * 列表
     * @return void
     */
    public function getFeedbackList($pageSize, $startDate, $endDate, $status)
    {
        $model = new Feedback();
        return $model
            ->where(function ($query) use ($startDate, $endDate, $status){
                if($startDate){
                    $query->where('create_time', '>=', strtotime($startDate. ' 00:00:00'));
                }
                if($endDate){
                    $query->where('create_time', '<=', strtotime($endDate.' 23:59:59'));
                }
                if ($status) {
                    $query->where('status', $status);
                }
            })
            ->order(['status'=>'asc','create_time'=>'desc'])
            ->paginate($pageSize)->each(function($item) {
                if (!empty($item['images'])) {
                    $item['images'] = json_decode((string)$item['images'], true);
                }
                return $item;
            });
    }

    /**
     * 更新数据
     * @param $data
     * @return void
     */
    public function edit($data)
    {
        if (empty($data)) {
            return false;
        }
        $model = new Feedback();
        try {
            if (isset($data['id'])) {
                $item = $model->find($data['id']);
                $data['status'] = 1;
                $item->save($data);
            }
            return true;
        } catch (\Exception $e) {
            Log::error("edit feedback error:". $e->getMessage());
            return false;
        }
    }

    /**
     * 删除记录
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        $model = new Feedback();
        return $model->where('id', $id)->delete();
    }

    /**
     * 获取总数
     * @return void
     */
    public function getFeedbackCount()
    {
        $model = new Feedback();
        return $model->where('status', 0)->count();
    }
}