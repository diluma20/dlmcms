<?php declare(strict_types=1);

namespace app\admin\validate;

class AdminValidate extends DilumaValidate
{
    /**
     * 规则
     * @var string[]
     */
    protected $rule =   [
        'id' => 'require',
        'role_id' => 'require',
        'username'  => 'require|alphaDash|unique:admin_user|length:5,50',
        'phone' => 'require|mobile|unique:admin_user',
        'password'  => 'require|length:6,20',
        'repassword'=>'require|confirm:password'
    ];

    /**
     * 提示信息
     * @var string[]
     */
    protected $message  =   [
        'id.require'   => '参数id丢失',
        'role_id.require'   => '请选择管理员所属角色',
        'username.require'   => '用户名不能为空',
        'username.alphaDash' => '用户名不能为空|用户名格式只能是字母、数字、——或_',
        'username.unique'   => '用户名已存在',
        'username.length'   => '用户名长度不对',
        'phone.require'     => '手机不能为空',
        'phone.mobile'     => '手机格式不正确',
        'phone.unique'     => '手机已存在',
        'password.require'   => '密码不能为空',
        'password.length'   => '密码长度只能在6-20之间',
        'repassword.require'    => '请输入重复密码',
        'repassword.confirm'    => '2次密码不一致',
    ];

    /**
     * 使用场景
     * @var array
     */
    protected $scene = [
        'insert' => ['role_id','username','phone','password','repassword'],
        'edit' => ['id','role_id', 'phone'],
    ];
}