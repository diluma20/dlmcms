<?php declare(strict_types=1);

namespace app\admin\validate;

class SiteValidate extends DilumaValidate
{
    /**
     * 规则
     * @var string[]
     */
    protected $rule =   [
        'id' => 'require',
        'site_flag' => 'require|alphaDash|unique:site',
        'site_name'  => 'require',
        'keywords' => 'require|max:255',
        'description' => 'require|max:500',
        'site_url' => 'require',
        //'email' => 'require|email',
        'mobile' => 'require|mobile',
        'address' => 'require',
        'icp'=>'require'
    ];

    /**
     * 提示信息
     * @var string[]
     */
    protected $message  =   [
        'id.require'   => '参数id丢失',
        'site_flag.require'   => '请输入网站标识并保证唯一性',
        'site_flag.alphaDash' => '网站标识不能为空|网站标识格式只能是字母、数字、——或_',
        'site_flag.unique'   => '网站标识已存在',
        'site_name.require'   => '网站名称不能为空',
        'keywords.require'     => '网站关键字不能为空',
        'keywords.max'     => '网站关键字长度最大255个字符',
        'description.require'     => '网站描述不能为空',
        'description.max'     => '网站描述长度最大500个字符',
        'site_url.require'     => '网站域名不能为空',
        //'email.require'     => '邮箱不能为空',
        //'email.email'     => '邮箱格式不正确',
        'mobile.require'     => '手机号码不能为空',
        'mobile.mobile'     => '手机格式不正确',
        'address.require'   => '联系地址不能为空',
        'icp.length'   => 'Icp备案号不能为空'
    ];

    /**
     * 使用场景
     * @var array
     */
    protected $scene = [
        'edit' => ['id', 'site_name', 'keywords', 'description', 'site_url', 'mobile','address', 'icp'],
    ];
}