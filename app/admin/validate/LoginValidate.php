<?php declare(strict_types=1);

namespace app\admin\validate;

class LoginValidate extends DilumaValidate
{
    /**
     * 规则
     * @var string[]
     */
    protected $rule =   [
        'username'  => 'require|alphaDash|length:5,50',
        'password'  => 'require|length:6,20',
        'captcha'   => 'require',
    ];

    /**
     * 提示信息
     * @var string[]
     */
    protected $message  =   [
        'username.require'   => '用户名不能为空',
        'username.alphaDash' => '用户名不能为空|用户名格式只能是字母、数字、——或_',
        'username.length'   => '用户名长度不对',
        'password.require'   => '密码不能为空',
        'password.length'   => '密码长度只能在6-20之间',
        'captcha.require'    => '验证码不能为空'
    ];

    /**
     * 使用场景
     * @var array
     */
    protected $scene = [
        'login' => ['username','password','captcha'],
    ];
}