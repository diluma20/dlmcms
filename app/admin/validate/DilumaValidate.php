<?php declare(strict_types=1);

namespace app\admin\validate;

use think\Validate;
use app\common\exception\ParamException;

class DilumaValidate extends Validate
{
    /**
     * 自定义验证规则
     * @var array
     */
    protected $regex = [
        'currency' => '/^\d+(\.\d+)?$/',//货币
        'email'=>'/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/', //邮箱
        'mobile' => '/^1[3456789]\d{9}$/', //手机
        'url'  =>  '/^http(s?):\/\/(?:[A-za-z0-9-]+\.)+[A-za-z]{2,4}(:\d+)?(?:[\/\?#][\/=\?%\-&~`@[\]\':+!\.#\w]*)?$/', //url路径
    ];

    /**
     * 自定义验证id是否为正整数
     * @param $value
     * @param $rule
     * @param $data
     * @param $field
     * @return bool
     */
    protected function isPositiveInt($value, $rule='', $data='', $field=''){

        if( is_numeric($value) && is_int($value + 0 ) && ($value+0) > 0){
            return true;
        } else{
            return false;
        }
    }

    /**
     * 自定义验证操作方法
     * @param $data
     * @return bool
     * @throws ParamException
     */
    public function checkData($data){
        if(!$this->batch()->check($data)){
            throw new ParamException([
                'msg'=>is_array($this->error) ? implode(';', $this->error) : $this->error,
            ]);
        } else {
            return true;
        }
    }

}