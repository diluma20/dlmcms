<?php declare(strict_types=1);

namespace app\admin\validate;

/**
 * 菜单验证类
 * Class MenuValidate
 * @package app\admin\validate
 */
class MenuValidate extends DilumaValidate{
    protected $rule = [
        'id'=>'require|isPositiveInt',
        'name'=>'require',
        'pid'=>'require|number',
        'is_display'=> 'require|isPositiveInt',
        'type'=> 'require|isPositiveInt',
        'sort'=>'isPositiveInt'
    ];

    protected $message = [
        'name'  =>  '菜单名称不能为空',
        'pid' =>  '顶级菜单不能为空',
        'is_display.require' =>  '菜单类型不能为空',
        'is_display.isPositiveInt' =>  '菜单类型不合法',
        'type.require' =>  '节点类型不能为空',
        'type.isPositiveInt' =>  '节点类型不合法',
        'sort' =>  '排序必须是数字',
    ];

    /**
     * 使用场景
     * @var array
     */
    protected $scene = [
        'insert' => ['name','pid','is_display','type','sort'],
        'update' => ['id','name','pid','is_display','type','sort'],
    ];
}