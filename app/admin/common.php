<?php
// 这是系统自动生成的公共文件


if (!function_exists('message')) {
    /**
     * 消息数组函数
     * @param string $msg 提示语
     * @param bool $success 是否成功
     * @param array $data 结果数据
     * @param int $code 错误码
     * @return array 返回消息对象
     */
    function message($msg = "操作成功", $success = true, $data = [], $code = 0)
    {
        $result = ['msg'=>$msg, 'data'=>$data];
        if ($success) {
            $result['code'] = 0;
        } else {
            $result['code'] = $code ? $code : -1; // 失败状态(可配置常用状态码)
        }
        return json($result);
    }
}

if (!function_exists('sort_id')) {
    /**
     * 获取排序id
     * @param string $table
     * @return int
     */
    function sort_id(string $table, string $database = null): int
    {
        if ($database) {
            $id = \think\facade\Db::connect($database)->name($table)->max('id');
        } else {
            $id = \think\facade\Db::name($table)->max('id');
        }

        if($id){
            return (int)$id + 1;
        } else{
            return 1;
        }
    }
}




