<?php declare(strict_types=1);

return [
    "middleware" => [
        \app\admin\middleware\Auth::class, // 判断用户是否登录
    ]
];