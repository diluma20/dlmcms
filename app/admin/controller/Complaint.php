<?php declare(strict_types=1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\service\ComplaintService;
use think\facade\View;
use think\Log;

class Complaint extends Diluma
{
    /**
     * 首页
     * @return mixed
     */
    public function index()
    {
        return View::fetch();
    }

    /**
     * 列表数据
     * @return void
     * */
    public function getComplaintList()
    {
        $startDate = $this->request->param('start_date');
        $endDate = $this->request->param('end_date');
        $status = $this->request->param('status');
        $pageSize = $this->request->param('page_size', 10);

        $service = new ComplaintService();
        $list = $service->getComplaintList((int)$pageSize, $startDate, $endDate, $status);

        return json(['code'=>0, 'count'=>$list->total(), 'data'=>$list->items()]);
    }

    /**
     * 编辑
     * @return \think\response\Json
     */
    public function edit(){
        $service = new ComplaintService();
        $post = $this->request->post();
        if(empty($post['id'])){
            return json(['code'=>1, 'msg'=>'参数异常']);
        }
        $res = $service->edit($post);
        if ($res) {
            return json(['code'=>0, 'msg'=>'编辑成功']);
        } else {
            return json(['code'=>1, 'msg'=>'编辑失败，稍后再试']);
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function delete(){
        $id = $this->request->param('id');
        if(!$id){
            return json(['code'=>1, 'msg'=>'参数异常']);
        }
        try {
            $service = new ComplaintService();
            $service->delete($id);
            return json(['code'=>0, 'msg'=>'删除成功']);
        } catch (\Exception $e) {
            Log::error("delete error:". $e->getMessage());
            return json(['code'=>1, 'msg'=>$e->getMessage()]);
        }
    }
}
?>