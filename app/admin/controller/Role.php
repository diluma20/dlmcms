<?php declare(strict_types=1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\model\Menu;
use app\admin\service\MenuService;
use app\admin\service\RoleService;
use think\facade\View;

/**
 * 角色控制器
 * Class Role
 * @package app\admin\controller
 */
class Role extends Diluma
{
    /**
     * 首页
     * @return string
     */
    public function index()
    {
        return View::fetch();
    }

    /**
     * 列表
     * @return \think\response\Json
     */
    public function getRoleList()
    {
        $pageSize = (int)$this->request->param('page_size', 10);
        $service = new RoleService();
        $list = $service->getRoleList($pageSize);

        return json(['code'=>0, 'count'=>$list->total(), 'data'=>$list->items()]);
    }

    /**
     * 添加角色
     * @return string
     */
    public function insert()
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $service = new RoleService();
            return $service->insert($post);
        } else {
            return View::fetch();
        }
    }

    /**
     * 角色编辑
     * @return \think\response\Json
     */
    public function edit()
    {
        $service = new RoleService();
        if ($this->request->post()) {
            return $service->edit($this->request->post());
        } else {
            $id = $this->request->param('id', 0, 'intval');
            $item = $service->getOneBy($id);
            View::assign('item', $item);
            return View::fetch();
        }
    }

    /**
     * 角色删除
     * @return [type] [description]
     */
    public function delete()
    {
        $service = new RoleService();
        $post = $this->request->post();
        if(empty($post['id'])){
            return json(['code'=>1, 'msg'=>'参数异常']);
        }

        return $service->delete((int)$post['id']);
    }
}
