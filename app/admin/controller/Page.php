<?php declare(strict_types=1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\model\Page as PageModel;
use app\admin\service\NavService;
use app\common\constant\RedisKeyConst;
use think\facade\Response;
use think\facade\Db;
use think\facade\View;
use think\facade\Log;
use think\cache\driver\Redis;

class Page extends Diluma
{
    /**
     * 单页
     * @return view
     */
    public function index()
    {
        return View::fetch();
    }

    /**
     * 列表数据
     * @return json
     * @throws \think\db\exception\DbException
     */
    public function getPageList()
    {

        $pageSize = (int)$this->request->param('page_size', 10);
        $model = new PageModel();
        $items = $model
            ->field(['id','name','status','update_time'])
            ->order(['update_time'=>'desc', 'id'=>'desc'])
            ->paginate($pageSize);
        return json(['code'=>0, 'count'=>$items->total(),'data'=>$items->items()]);
    }

    /**
     * 修改显示隐藏
     * @return array|\think\response\Json
     */
    public function updatePageStatus(){
        $id = $this->request->param('id');
        $status = $this->request->param('status');
        if (empty($id) ) {
            return ["code" => 1, 'msg' => '操作行记录id丢失'];
        }
        $model = new PageModel();
        Db::startTrans();
        try {
            $item = $model->find($id);
            if (empty($item)) {
                return json(['code'=>1, 'msg'=>'记录不存在']);
            }
            $item->save(['id'=>$id, 'status'=>$status]);
            Db::commit();
            return json(['code'=>0, 'msg'=>'修改成功']);
        } catch (\Exception $e) {
            Log::error("updatePageStatus failed:". $e->getMessage());
            Db::rollback();
            return json(['code'=>1, 'msg'=>'修改失败，'. $e->getMessage()]);
        }
    }

    /**
     * 添加
     * @return mixed|Json
     * */
    public function insert(){
        if ($this->request->isPost()) {
            $model = new PageModel();
            $post = $this->request->post(['field']);
            Db::startTrans();
            try {
                $data = [
                    'name' => $post['field']['name'],
                    'nav_id' => $post['field']['nav_id'],
                    'content' => $post['field']['content']
                ];
                $model->save($data);
                Db::commit();
                return json(['code'=>0, 'msg'=>'新增成功']);
            } catch (\Exception $e) {
                Log::error("insert page failed:". $e->getMessage());
                Db::rollback();
                return json(['code'=>1, 'msg'=>'新增失败，'. $e->getMessage()]);
            }
        } else{
            $service = new NavService();
            $navItems = $service->getNavList();
            View::assign('nav_list', $navItems);
            return View::fetch();
        }
    }

    /**
     * 编辑
     * @return string|\think\response\Json
     */
    public function edit()
    {
        $model = new PageModel();
        if($this->request->isPost()) {
            $post = $this->request->post();
            Db::startTrans();
            try {
                $item = $model->find($post['id']);
                if (empty($item)) {
                    return json(['code'=>1, 'msg'=>'记录不存在']);
                }
                if(false === $item->save($post)) {
                    Db::rollback();
                    return json(['code'=>1, 'msg'=>'修改失败，请稍后再试']);
                }
                Db::commit();
                return json(['code'=>0, 'msg'=>'修改成功']);
            } catch (\Exception $e) {
                Log::error("edit page failed:". $e->getMessage());
                Db::rollback();
                return json(['code'=>1, 'msg'=>'修改失败，'. $e->getMessage()]);
            }
        } else {
            $id = $this->request->param('id');
            $item = $model->find($id);
            $service = new NavService();
            $navItems = $service->getNavList();
            View::assign('nav_list', $navItems);
            View::assign('item', $item);
            return View::fetch();
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function delete(){
        $post = $this->request->post();
        if(empty($post['id'])){
            return json(['code'=>0, 'msg'=>'参数异常']);
        }

        $model = new PageModel();
        try {
            $model->where('id', $post['id'])->delete();
            return json(['code'=>0, 'msg'=>'删除成功']);
        } catch (\Exception $e) {
            return json(['code'=>1, 'msg'=>'删除失败，'. $e->getMessage()]);
        }
    }
}