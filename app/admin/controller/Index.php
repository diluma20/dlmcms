<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\service\FeedbackService;
use app\admin\service\MessageService;
use think\facade\App;
use think\facade\Db;
use think\facade\View;

class Index extends Diluma
{
    /**
     * 主页
     * @return mixed
     */
    public function main(){
        $mysqlVersion = Db::query('SELECT VERSION() AS ver');
        $sysInfo = [
            'url'             => $_SERVER['HTTP_HOST'],
            'document_root'   => $_SERVER['DOCUMENT_ROOT'],
            'server_os'       => PHP_OS,
            'server_port'     => $_SERVER['SERVER_PORT'],
            'server_ip'       => isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '',
            'server_soft'     => $_SERVER['SERVER_SOFTWARE'],
            'php_version'     => PHP_VERSION,
            'mysql_version'   => $mysqlVersion[0]['ver'],
            'max_upload_size' => ini_get('upload_max_filesize'),
            'version'         => App::version()
        ];
        // 建议
        View::assign('feedback_count', (new FeedbackService())->getFeedbackCount());
        // 留言
        View::assign('msg_count', (new MessageService())->getMsgCount());
        View::assign('_name', _NAME);
        View::assign('_version', VERSION);
        View::assign('_site_url', SITE_URL);
        View::assign("config", $sysInfo);
        return View::fetch();
    }

    /**
     * 后台主入口
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index(){

        View::assign('_site_url', SITE_URL);
        return View::fetch();
    }
}
