<?php declare(strict_types=1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\service\NavService;
use app\common\helper\ConstBase;
use think\facade\View;

class Nav extends Diluma
{
    /**
     * 首页
     * @return string
     */
    public function index()
    {
        return View::fetch();
    }

    /**
     * 列表数据
     * @return \think\response\Json
     */
    public function getNavList(){
        $service = new NavService();
        $items = $service->getNavList();
        return json(['code'=>0, 'data'=>$items]);
    }

    /**
     * 排序
     * @return mixed
     * @throws \think\db\exception\DbException
     */
    public function sort(): mixed
    {
        $post = $this->request->post();
        $service = new NavService();
        return $service->sort($post);
    }

    /**
     * 新增子导航
     * @return mixed|void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function insert(){
        $service = new NavService();
        if($this->request->isPost()) {
            $post = $this->request->post();
            $item = $service->getOneBy($post['field']['name']);
            if (!empty($item)) {
                return json(["code" => 1, 'msg' => "{$post['field']['name']} 已存在，请勿重复添加"]);
            }
            $ret = $service->updateOrInsert($post['field']);
            if($ret) {
                return json(["code" => 0, 'msg' => '添加成功']);
            } else {
                return json(["code" => 1, 'msg' => '更新失败，请重试']);
            }
        } else {
            $parentId = $this->request->param('parent_id');
            if(!$parentId) {
                $parentId = 0;
            }
            view::assign('parent_id', $parentId);
            $navItems = $service->getNavList();
            View::assign('nav_type', ConstBase::NAV_TYPE);
            View::assign('nav_list', $navItems);
            view::assign('sort', $service->getSort());
            return view::fetch();
        }
    }

    /**
     * 修改导航
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit()
    {
        $service = new NavService();
        if($this->request->isPost()) {
            $id = $this->request->param('field.id', 0, 'intval');
            if(!$id){
                return json(['code'=>1, 'msg'=>'id不正确']);
            }
            $post = $this->request->post();

            $item = $service->getOne($id);
            if(empty($item)) {
                return json(['code'=>1, 'msg'=>'记录不存在']);
            }

            if(false == $service->updateOrInsert($post['field'])) {
                return json(['code'=>1, 'msg'=>'修改失败']);
            } else {
                return json(['code'=>0, 'msg'=>'修改成功']);
            }
        } else{
            $id = $this->request->param('id', 0, 'intval');
            if($id <= 0){
                return josn(['code'=>1, 'msg'=>'参数异常']);
            }
            $navItems = $service->getNavList();
            $nav = $service->getOne($id);
            View::assign('nav_type', ConstBase::NAV_TYPE);
            View::assign('nav_list', $navItems);
            View::assign('item', $nav);
            return View::fetch();
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function delete(){
        $post = $this->request->post();
        if(empty($post['id'])){
            return json(['code'=>0, 'msg'=>'参数异常']);
        }

        $service = new NavService();
        return $service->delete($post['id']);
    }
}