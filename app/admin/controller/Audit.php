<?php declare(strict_types=1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\service\AuditService;
use think\facade\View;

class Audit extends Diluma
{
    public function index(){
        return View::fetch('user/audit');
    }

    /**
     * 认证列表
     * @return void
     */
    public function getUserAuditList(){
        $startDate = $this->request->param('start_date');
        $endDate = $this->request->param('end_date');
        $keywords = $this->request->param('keywords');
        $status = $this->request->param('status');
        $type = $this->request->param('type', 1);
        $pageSize = $this->request->param('page_size', 10);

        $service = new AuditService();
        $items = $service->getUserAuditList((int)$pageSize, $startDate, $endDate, $keywords, $status, $type);
        return json(['code'=>0, 'count'=>$items->total(), 'data'=>$items->items()]);
    }

    /**
     * 认证审核
     * @return void
     */
    public function auth(){
        $service = new AuditService();
        if($this->request->isPost()) {
            $post = $this->request->post();
            if (empty($post['id']) || empty($post['status'])) {
                return json(['code'=>1, 'msg'=>'参数异常']);
            }
            switch ($post['type']){
                case "1":
                    return $service->authUserIdCardStatus($post);
            }
        } else {
            $id = $this->request->param('id');
            $type = $this->request->param('type', 1);
            switch ($type) {
                case 1:
                    $item = $service->getUserIdCradAuditItem($id);
                    break;
            }
            View::assign('item',$item);
            return View::fetch('user/audit_edit');
        }
    }

}