<?php declare(strict_types=1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\service\BannerCategoryService;
use app\admin\service\BannerService;
use think\facade\Response;
use think\facade\View;
use think\facade\Log;

class Banner extends Diluma
{
    /**
     * 首页
     * @return view
     */
    public function index()
    {
        return View::fetch();
    }

    /**
     * 列表数据
     * @return json
     */
    public function getBannerList(){
        $pageSize = (int)$this->request->param('page_size', 10);

        $service = new BannerService();
        $list = $service->getBannerList($pageSize);

        return json(['code'=>0, 'count'=>$list->total(), 'data'=>$list->items()]);
    }

    /**
     * 添加
     * @return mixed|Json
     * */
    public function insert(){
        if ($this->request->isPost()) {
            $service = new BannerService();
            $post = $this->request->post(['field']);
            $data = [
                'category_id' => $post['field']['category_id'],
                'name' => $post['field']['name'],
                'image_url' => $post['field']['image_url'],
                'link_url' => $post['field']['link_url'],
                'status' => $post['field']['status']
            ];
            return $service->updateOrInsert($data);
        } else{
            $categoryItems = (new BannerCategoryService())->getBannerCategoryList();
            View::assign("category_list", $categoryItems);
            return View::fetch();
        }
    }

    /**
     * 编辑
     * @return string|\think\response\Json
     */
    public function edit()
    {
        $id = $this->request->param('id');
        $service = new BannerService();
        $item = $service->getOne($id);
        if ($this->request->isPost()) {
            $post = $this->request->post();
            if(empty($post['field']['id']) || empty($post['field']['image_url'])){
                return json(['code'=>1, 'msg'=>'参数异常']);
            }
            return $service->updateOrInsert($post['field']);
        } else{
            $categoryItems = (new BannerCategoryService())->getBannerCategoryList();
            View::assign("category_list", $categoryItems);
            View::assign('item', $item);
            return View::fetch();
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function delete(){
        $id = $this->request->param('id');
        if(!$id){
            return json(['code'=>1, 'msg'=>'参数异常']);
        }
        try {
            $service = new BannerService();
            $service->delete($id);
            return json(['code'=>0, 'msg'=>'删除成功']);
        } catch (\Exception $e) {
            Log::error('delete error:'. $e->getMessage());
            return json(['code'=>1, 'msg'=>$e->getMessage()]);
        }
    }
}