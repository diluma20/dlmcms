<?php declare(strict_types=1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\service\OrderService;
use think\facade\View;

class Order extends Diluma
{
    public function index(){
        return View::fetch();
    }

    /**
     * 订单列表
     * @return void
     */
    public function getOrderList(){
        $startDate = $this->request->param('start_date');
        $endDate = $this->request->param('end_date');
        $orderSn = $this->request->param('order_sn');
        $userId = $this->request->param('user_id');
        $pageSize = $this->request->param('page_size', 10);
        $service = new OrderService();
        $items = $service->getOrderList($startDate, $endDate, $pageSize, $orderSn, $userId);
        return json(['code'=>0, 'count'=>$items->total(), 'data'=>$items->items()]);
    }

    /**
     * 订单进度
     * @return void
     */
    public function detail(){
        $id = $this->request->param('id');
        $service = new OrderService();
        $item = $service->getOne($id);
        View::assign('item', $item);
        return View::fetch();
    }
}