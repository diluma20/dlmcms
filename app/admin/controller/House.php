<?php declare(strict_types=1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\service\HouseService;
use app\admin\service\TagService;
use app\common\helper\ConstBase;
use think\facade\View;

class House extends Diluma
{
    /**
     * 首页
     * @return mixed
     */
    public function index()
    {
        return View::fetch();
    }

    /**
     * 列表数据
     * @return \think\response\Json
     */
    public function getDataList(){
        $keywords = $this->request->param('keywords');
        $pageSize = $this->request->param('page_size', 10, 'intval');

        $service = new HouseService();
        $list = $service->getDataList($keywords, $pageSize);

        return json(['code'=>0, 'count'=>$list->total(), 'data'=>$list->items()]);
    }

    /**
     * 添加
     * @return string
     */
    public function insert(){
        $service = new HouseService();
        if ($this->request->isPost()) {
            $post = $this->request->post();
            return $service->updateOrInsert($post['field']);
        } else{
            $item['tag_list'] = (new TagService())->getAllTags(2);
            View::assign('item', $item);
            return View::fetch();
        }
    }

    /**
     * 编辑
     * @param int [id]
     * @param array[name|status|...]
     * @return mixed [boolean|array]
     */
    public function edit(){
        $service = new HouseService();
        if ($this->request->isPost()) {
            $post = $this->request->post();
            return $service->updateOrInsert($post['field']);
        } else{
            if (!$this->request->param('id')) {
                return json(['code'=>1, 'msg'=>'id不存在，请稍后再试']);
            }
            $item = $service->getOne((int)$this->request->param('id'));
            $item['tag_list'] = (new TagService())->getAllTags(2);
            if (!empty($item['images_url'])) {
                $item['images_urls'] = explode(';', rtrim($item['images_url'], ';'));
            }
            View::assign('item', $item);
            return View::fetch();
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function delete(){
        $post = $this->request->post();
        if(empty($post['id'])){
            return json(['code'=>0, 'msg'=>'参数异常']);
        }
        $id = $post['id'];
        try {
            $service = new HouseService();
            $service->delete($id);
            return json(['code'=>0, 'msg'=>'删除成功']);
        } catch (\Exception $e) {
            return json(['code'=>1, 'msg'=>$e->getMessage()]);
        }
    }
}
?>