<?php declare(strict_types=1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\service\CasesService;
use app\admin\service\TagService;
use app\common\helper\ConstBase;
use think\facade\View;

class Cases extends Diluma
{
    /**
     * 案例
     * @return mixed
     */
    public function index()
    {
        $service = new CasesService();
        $categoryItems = $service->getCasesCategoryList();
        View::assign('category_list', $categoryItems);
        return View::fetch();
    }

    /**
     * 列表数据
     * @return \think\response\Json
     */
    public function getCasesList(){
        $keywords = $this->request->param('keywords');
        $categoryId = $this->request->param('category_id');
        $pageSize = $this->request->param('page_size', 10, 'intval');

        $service = new CasesService();
        $list = $service->getCasesList($keywords, $categoryId, $pageSize);

        return json(['code'=>0, 'count'=>$list->total(), 'data'=>$list->items()]);
    }

    /**
     * 添加
     * @return string
     */
    public function insert(){
        $service = new CasesService();
        if ($this->request->isPost()) {
            $post = $this->request->post();
            return $service->updateOrInsert($post['field']);
        } else{
            $categoryItems = $service->getCasesCategoryList();
            $item['category_list'] = $categoryItems;
            $item['tag_list'] = (new TagService())->getAllTags(1);
            $item['designer_list'] = $service->getDesignerList();
            View::assign('item', $item);
            return View::fetch();
        }
    }

    /**
     * 编辑
     * @param int [id]
     * @param array[name|status|...]
     * @return mixed [boolean|array]
     */
    public function edit(){
        $service = new CasesService();
        if ($this->request->isPost()) {
            $post = $this->request->post();
            return $service->updateOrInsert($post['field']);
        } else{
            if (!$this->request->param('id')) {
                return json(['code'=>1, 'msg'=>'id不存在，请稍后再试']);
            }
            $item = $service->getOne((int)$this->request->param('id'));
            $categoryList = $service->getCasesCategoryList();
            $item['category_list'] = $categoryList;
            $item['tag_list'] = (new TagService())->getAllTags(1);
            $item['designer_list'] = $service->getDesignerList();
            if (!empty($item['images_url'])) {
                $item['images_urls'] = explode(';', rtrim($item['images_url'], ';'));
            }
            View::assign('item', $item);
            return View::fetch();
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function delete(){
        $post = $this->request->post();
        if(empty($post['id'])){
            return json(['code'=>0, 'msg'=>'参数异常']);
        }
        $id = $post['id'];
        try {
            $service = new CasesService();
            $service->delete($id);
            return json(['code'=>0, 'msg'=>'删除成功']);
        } catch (\Exception $e) {
            return json(['code'=>1, 'msg'=>$e->getMessage()]);
        }
    }

    /**
     * 分类页
     * @return mixed
     */
    public function category()
    {
        return View::fetch();
    }

    /**
     * 案例分类
     * @return \think\response\Json
     */
    public function getCategoryList(){
        $pageSize = $this->request->param('page_size', 10, 'intval');
        $service = new CasesService();
        $item = $service->getCasesCategoryList($pageSize);

        return json(['code'=>0, 'count'=>$item->total(), 'data'=>$item->items()]);
    }

    /**
     * 添加分类
     * @return string|\think\response\Json
     */
    public function categoryInsert(){
        if ($this->request->isPost()) {
            $post = $this->request->post();
            return (new CasesService())->categoryUpdateOrInsert($post['field']);
        } else {
            return View::fetch();
        }
    }

    /**
     * 编辑分类
     * @return string|void
     */
    public function categoryEdit(){
        if ($this->request->isPost()) {
            $post = $this->request->post();
            return (new CasesService())->categoryUpdateOrInsert($post['field']);
        } else {
            $id = $this->request->get()['id'];
            $item = (new CasesService())->getCategoryBy($id);
            View::assign('item', $item);
            return View::fetch();
        }
    }

    /**
     * 分类删除：删除分类必须清空该分类下所有的文章
     * @return \think\response\Json
     */
    public function categoryDelete(){
        $post = $this->request->post();
        if(empty($post['id'])){
            return json(['code'=>1, 'msg'=>'参数异常']);
        }

        return (new CasesService())->categoryDelete($post['id']);
    }
}
?>