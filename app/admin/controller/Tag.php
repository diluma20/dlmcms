<?php declare(strict_types=1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\service\TagService;
use think\facade\View;

class Tag extends Diluma
{
    /**
     * 标签管理
     * @return mixed
     */
    public function index()
    {
        return View::fetch();
    }

    /**
     * 列表数据
     * @return mixed
     * */
    public function getTagList(){
        $type = (int)$this->request->param('type', 1);
        $pageSize = (int)$this->request->param('page_size', 10);

        $service = new TagService();
        $list = $service->getTagList($type, $pageSize);

        return json(['code'=>0, 'count'=>$list->total(), 'data'=>$list->items()]);
    }

    /**
     * 编辑
     * @return \think\response\Json
     */
    public function edit(){
        $service = new TagService();
        $post = $this->request->post();
        if(empty($post['name'])){
            return json(['code'=>1, 'msg'=>'参数异常']);
        }
        $res = $service->updateOrInsert($post);
        if ($res) {
            return json(['code'=>0, 'msg'=>'编辑成功']);
        } else {
            return json(['code'=>1, 'msg'=>'编辑失败，稍后再试']);
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function delete(){
        $post = $this->request->post();
        if(empty($post['id'])){
            return json(['code'=>0, 'msg'=>'参数异常']);
        }
        $id = $post['id'];
        try {
            $service = new TagService();
            $service->delete($id);
            return json(['code'=>0, 'msg'=>'删除成功']);
        } catch (\Exception $e) {
            return json(['code'=>1, 'msg'=>$e->getMessage()]);
        }
    }
}
?>