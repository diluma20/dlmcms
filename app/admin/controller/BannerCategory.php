<?php declare(strict_types=1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\service\BannerCategoryService;
use think\facade\Response;
use think\facade\View;
use think\facade\Log;

class BannerCategory extends Diluma
{
    /**
     * 首页
     * @return view
     */
    public function index()
    {
        return View::fetch('banner/category');
    }

    /**
     * 列表数据
     * @return json
     */
    public function getBannerCategoryList(){
        $pageSize = (int)$this->request->param('page_size', 10);

        $service = new BannerCategoryService();
        $list = $service->getBannerCategoryList($pageSize);

        return json(['code'=>0, 'count'=>$list->total(), 'data'=>$list->items()]);
    }

    /**
     * 添加分类
     * @return mixed
     * */
    public function insert(){
        if ($this->request->isPost()) {
            $service = new BannerCategoryService();
            $post = $this->request->post(['field']);
            $data = [
                'name' => $post['field']['name'],
                'status' => $post['field']['status']
            ];
            return $service->updateOrInsert($data);
        } else{
            return View::fetch('banner/category_insert');
        }
    }

    /**
     * 编辑
     * @return string|\think\response\Json
     */
    public function edit()
    {
        $id = $this->request->param('id');
        $service = new BannerCategoryService();
        $item = $service->getOne($id);
        if ($this->request->isPost()) {
            $post = $this->request->post();
            if(empty($post['field']['id']) || empty($post['field']['name'])){
                return json(['code'=>1, 'msg'=>'参数异常']);
            }
            return $service->updateOrInsert($post['field']);
        } else{
            View::assign('item', $item);
            return View::fetch('banner/category_edit');
        }
    }

    /**
     * 删除分类
     * @return \think\response\Json
     */
    public function delete(){
        $id = $this->request->param('id');
        if(!$id){
            return json(['code'=>1, 'msg'=>'参数异常']);
        }
        try {
            $service = new BannerCategoryService();
            $ret = $service->delete($id);
            return json(['code'=>$ret['code'], 'msg'=>$ret['msg']]);
        } catch (\Exception $e) {
            Log::error('delete error:'. $e->getMessage());
            return json(['code'=>1, 'msg'=>$e->getMessage()]);
        }
    }
}