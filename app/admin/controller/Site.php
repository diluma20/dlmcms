<?php declare(strict_types=1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\validate\SiteValidate;
use think\exception\ValidateException;
use think\facade\Log;
use think\facade\View;
use app\admin\service\SiteService;

class Site extends Diluma
{

    /**
     * 首页
     * @return string
     */
    public function index()
    {
        return View::fetch();
    }

    /**
     * 网站配置
     * @return \think\response\Json
     */
    public function getSiteConfig()
    {
        $service = new SiteService();
        $item = $service->getSiteConfig();
        return json(['code'=>0, 'msg'=>'success', 'data'=>$item]);
    }

    /**
     * 编辑管理员
     * @return \think\response\Json
     */
    public function edit()
    {
        $service = new SiteService();
        $post = $this->request->post();
        try {
            validate(SiteValidate::class)->scene('edit')->checkData($post['field']);
        } catch (ValidateException $e) {
            return json(['code' => 500, 'msg' => $e->getError()]);
        }

        return $service->updateOrInsert($post['field']);
    }
}