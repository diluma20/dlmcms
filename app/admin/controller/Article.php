<?php declare(strict_types=1);

namespace app\admin\controller;

use app\admin\Diluma;
use app\admin\service\ArticleService;
use app\common\helper\ConstBase;
use think\facade\View;

class Article extends Diluma
{
    /**
     * 首页
     * @return string
     */
    public function index()
    {
        $service = new ArticleService();
        $categoryItems = $service->getArticleCategoryList();
        View::assign('category_list', $categoryItems);
        return View::fetch();
    }

    /**
     * 文章列表
     * @return void
     */
    public function getArticleList(){
        $service = new ArticleService();
        $pageSize = (int)$this->request->param('page_size', 10);
        $startDate = $this->request->param('start_date');
        $endDate = $this->request->param('end_date');
        $articleId = $this->request->param('article_id');
        $keywords = $this->request->param('keywords');
        $editor = $this->request->param('editor');
        $categoryId = $this->request->param('category_id');

        $item = $service->getArticleList($pageSize, $startDate, $endDate, $articleId, $keywords, $editor, $categoryId);

        return json(['code'=>0, 'count'=>$item->total(), 'data'=>$item->items()]);
    }

    /**
     * 文章操作（显示/隐藏）
     * @return \think\response\Json
     */
    public function action(){
        $post = $this->request->post();

        if(!$post['id']){
            return json(['code'=>1, 'msg'=>'参数异常']);
        }

        return (new ArticleService())->action($post['id'], $post['type']);
    }

    /**
     * 新增
     * @return void
     */
    public function insert(){
        $service = new ArticleService();
        if ($this->request->isPost()) {
            $post = $this->request->post();
            return $service->updateOrInsert($post['field']);
        } else{
            $categoryItems = $service->getArticleCategoryList();
            $item['category_list'] = $categoryItems;
            View::assign('item', $item);
            return View::fetch();
        }
    }

    /**
     * 编辑
     * @return void
     */
    public function edit(){
        $service = new ArticleService();
        if ($this->request->isPost()) {
            $post = $this->request->post();
            return $service->updateOrInsert($post['field']);
        } else{
            if (!$this->request->param('id')) {
                return json(['code'=>1, 'msg'=>'文章id不存在，请稍后再试']);
            }
            $item = $service->getOne((int)$this->request->param('id'));
            $categoryList = $service->getArticleCategoryList();
            $item['category_list'] = $categoryList;
            View::assign('item', $item);
            return View::fetch();
        }
    }

    /**
     * 文章删除（单个/多个）
     * @return void
     */
    public function delete(){
        $post = $this->request->post();
        if(empty($post['id'])){
            return json(['code'=>1, 'msg'=>'参数异常']);
        }

        return (new ArticleService())->delete($post['id']);
    }

    /**
     * 分类页
     * @return mixed
     */
    public function category()
    {
        return View::fetch();
    }

    /**
     * 文章分类
     * @return \think\response\Json
     */
    public function getCategoryList(){
        $pageSize = (int)$this->request->param('page_size', 10);
        $service = new ArticleService();
        $item = $service->getArticleCategoryList($pageSize);

        return json(['code'=>0, 'count'=>$item->total(), 'data'=>$item->items()]);
    }

    /**
     * 分类删除：删除分类必须清空该分类下所有的文章
     * @return \think\response\Json
     */
    public function categoryDelete(){
        $post = $this->request->post();
        if(empty($post['id'])){
            return json(['code'=>1, 'msg'=>'参数异常']);
        }

        return (new ArticleService())->categoryDelete($post['id']);
    }

    /**
     * 添加分类
     * @return string|\think\response\Json
     */
    public function categoryInsert(){
        if ($this->request->isPost()) {
            $post = $this->request->post();
            return (new ArticleService())->categoryUpdateOrInsert($post['field']);
        } else {
            $types = ConstBase::ARTICLE_TYPE;
            View::assign('type_list', $types);
            return View::fetch();
        }
    }

    /**
     * 编辑分类
     * @return string|void
     */
    public function categoryEdit(){
        if ($this->request->isPost()) {
            $post = $this->request->post();
            return (new ArticleService())->categoryUpdateOrInsert($post['field']);
        } else {
            $id = $this->request->get()['id'];
            $item = (new ArticleService())->getCategoryBy($id);
            $types = ConstBase::ARTICLE_TYPE;
            View::assign('type_list', $types);
            View::assign('item', $item);
            return View::fetch();
        }
    }
}