<?php declare(strict_types=1);

namespace app\admin\middleware;

use think\Response;

class Auth
{
    /**
     * 处理请求
     * @param \think\Request $request
     * @param \Closure $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        $controller = $request->controller(true);
        if (strtolower($controller) != 'login' && empty(session('{ADMIN_LOGIN_INFO}'))) {
            return redirect('/admin/login/login');
        }

        return $next($request);
    }
}