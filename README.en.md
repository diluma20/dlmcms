# DLMCMS

#### Description
的卢马CMS是由 的卢信息技术（上海）有限公司的 的卢马团队，以下简称（的卢马）研发的一款开源的后台CMS系统。系统采用流行的TP8框架为驱动，PHP为8.1.0以上版本，数据库为MySQL5.7，前端以Layui框架为基础。系统包含了系统设置、菜单、权限分配、Banner、文章管理等基础功能。第一个版本数据保存在MySQL中，后续优化版本将采用Redis+MySQL保存数据，提高网站的访问速度。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
