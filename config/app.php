<?php
// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------

return [
    // 应用地址
    'app_host'         => env('APP_HOST', ''),
    // 应用的命名空间
    'app_namespace'    => '',
    // 是否启用路由
    'with_route'       => true,
    // 默认应用
    'default_app'      => 'home',
    // 默认时区
    'default_timezone' => 'Asia/Shanghai',

    // 应用映射（自动多应用模式有效）
    'app_map'          => [],
    // 域名绑定（自动多应用模式有效）
    'domain_bind'      => [],
    // 禁止URL访问的应用列表（自动多应用模式有效）
    'deny_app_list'    => ['common'],

    // 异常页面的模板文件
    'exception_tmpl'   => app()->getThinkPath() . 'tpl/think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'    => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'   => true,
    // 配置邮件
    'email'   => [
        'host'     => 'smtp.qq.com', // SMTP服务器地址
        'port'     => 465, // SMTP服务器端口
        'username' => '1586498033@qq.com', // 邮箱用户名
        'password' => '158649Sf', // 邮箱密码
        'secure'   => 'ssl', // 安全协议
        'from'     => '1586498033@qq.com',
    ],
    'receive_email' => '965516652@qq.com',
];
