[![](https://diluma.oss-cn-shanghai.aliyuncs.com/logo/logo2.png)](https://www.848981.net)

DLMCms

#### 介绍
的卢马CMS是由 的卢信息技术（上海）有限公司的 的卢马团队，以下简称（的卢马）研发的一款开源的后台CMS系统。系统采用流行的TP8框架为驱动，PHP为8.1.0以上版本，数据库为MySQL5.7，前端以Layui框架为基础。系统包含了系统设置、菜单、权限分配、Banner、文章、案例管理、下载等基础功能。第一个版本数据保存在MySQL中，后续优化版本将采用Redis+MySQL保存数据，提高网站的访问速度。

## 特性

* 基于PHP`8.0+`重构
* 升级`PSR`依赖
* 依赖`think-orm`3.0版本
* MySQL >= 5.7
* Redis >= 5.0

> ThinkPHP8.0的运行环境要求PHP8.0.0+

## 文档

[完全开发手册](https://doc.thinkphp.cn)

## 服务

ThinkPHP生态服务由[顶想云](https://www.topthink.com)（TOPThink Cloud）提供，为生态提供专业的开发者服务和价值之选。

## 后台演示

功能包括：系统设置（菜单、角色、权限分配），参数配置（前端导航配置、行政区域、行业分类、岗位分类等），素材管理（文章、下载、案例、Banner管理等）、单页管理，用户管理等基础功能模块

![](https://diluma.oss-cn-shanghai.aliyuncs.com/demo/menu.png)

![](https://diluma.oss-cn-shanghai.aliyuncs.com/demo/nav.png)]

![](https://diluma.oss-cn-shanghai.aliyuncs.com/demo/article.png)

![](https://diluma.oss-cn-shanghai.aliyuncs.com/demo/page.png)

![](https://diluma.oss-cn-shanghai.aliyuncs.com/demo/user.png)

## 安装

~~~
composer create-project topthink/think tp
~~~

启动服务

~~~
cd tp
php think run
~~~

然后就可以在浏览器中访问

~~~
http://localhost:8000
~~~

如果需要更新框架使用
~~~
composer update topthink/framework
~~~

# DLMCMS


#### 安装教程

1.  通过 gitee:https://gitee.com/diluma20/dlmcms/tree/dev 直接下载即可
2.  通过 github:
3.  或通过官网 [848981.net](https://www.848981.net) 下载

#### 使用说明

1. 下载完整代码或是压缩包（解压）后放在自己的项目目录中
2. 创建数据库并导入 diluma.sql
3. .env 文件中修改数据库用户名和密码即可
4. 更新 composer 到最新版本
~~~
composer self-update
~~~
5. 登录后台：{域名}/admin/login/login，默认用户名（admin），密码(123456))
6. 在项目根目录下执行命令：
~~~
 composer update 或者 composer install
~~~

## Nginx配置主机
~~~~
server {
    listen 80;
    server_name www.848981.net;
    root /www/dlmcms/public;

    location / {
            index index.php index.html index.htm;
    }

    location ~ \.php(.*)$ {
        fastcgi_index index.php;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        #nginx支持tp
        fastcgi_param   PATH_INFO $1;
        include         fastcgi_params;
    }
}
~~~~
#### 配置好重载：nginx -s reload

## 伪静态配置
~~~~
location / {
   if (!-e $request_filename){
      rewrite  ^(.*)$  /index.php?s=$1  last;   break;
   }
}
~~~~

### 使用中如有任何问题请联系

* 微信：diluma0503
* 邮箱：1586498033@qq.com
* 手机：19156292659